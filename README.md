lazybrake
=========

lazybrake is a command-line wrapper for various DVD/BD rippers, ie.
HandBrakeCLI, that streamlines and automates ripping. It can automate ripping
of many discs, either of movies or TV series, by just starting the rip
automatically when a disc is inserted and then ejecting it once the ripping is
done, ready to accept a new disc. For TV series it will rip individual episodes
(if possible), and for movies it will rip the main title. You can also manually
provide it with a list of titles to rip, useful for certain stubborn discs.

Dependencies
------------

lazybrake needs Raku (see below) as well as
[HandBrakeCLI](https://handbrake.fr/) to be installed. It also uses `mediainfo`
and `udevdm`, which are all usually installed on your average GNU/Linux
distribution or should be easy to install. The raku module `JSON::Fast` is als
required. This can easily be installed through the command `zef install
JSON::Fast` once you have `raku` installed.  Optionally additional backends can
use other tools as well, see the manpage.

### Raku

lazybrake is written in [Raku](http://raku.org). Most distributions
have raku by now (some still package `raku` as `perl6` still). If yours
doesn't, or you want a more up-to-date raku, you can install with either
`rakudo-pkg` or `rakudobrew`, see below for more information. Of the two,
rakudo-pkg is by far the simplest and fastest, but requires root.

#### rakudo-pkg

`rakudo-pkg` packages up-to-date `raku` for many distributions, including
Fedora/CentOS, Debian/Ubuntu and openSUSE. See the [rakudo-pkg
README](https://github.com/nxadm/rakudo-pkg#os-repositories) for instructions
on how to add these repositories.

#### rakudobrew

See the [rakudobrew git repository](https://github.com/tadzik/rakudobrew) for
detailed instructions, but basically it boils down to this:

```
git clone https://github.com/tadzik/rakudobrew ~/.rakudobrew
export PATH="~/.rakudobrew/bin:$PATH"
rakudobrew init # Instructions for permanently adding it to your PATH
rakudobrew build moar # Install raku
```
