#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI et.al. that streamlines
# and automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend;
use LazyBrake::DVDChapter;
use LazyBrake::DVDChapterSet;
use LazyBrake::DiscTitle;
use LazyBrake::EntryDuration;
use LazyBrake::RipStatus;
use LazyBrake::backend::HDB::Role;
#| Backend: HandBrakeCLI
unit class LazyBrake::backend::HDB does LazyBrake::backend does LazyBrake::backend::HDB::Role;
use JSON::Fast;

has Str $.extension;
has @.dependencies = (['HandBrakeCLI','flatpak'],'mediainfo');
has $!HDBVersionValidated = False;
has $!HDBUsesMultiPass = False;

method TWEAK ()
{
    $!extension = self.getHDBPresetInfo.<ext>;
}

#| Checks our dependencies
method usable () returns Bool
{
    if !self.inPath('HandBrakeCLI') && !self.isFlatpakInstalled('fr.handbrake.ghb')
    {
        return False;
    }
    if !self.inPath('mediainfo')
    {
        return False;
    }
    return True;
}

method !HDBBaseCommand() returns Array
{
    # The flatpak is supported and recommended by upstream HandBrake
    if self.isFlatpakInstalled('fr.handbrake.ghb')
    {
        return self.getFlatpakCommand('fr.handbrake.ghb','HandBrakeCLI');
    }
    if self.inPath('HandBrakeCLI')
    {
        return 'HandBrakeCLI';
    }
    die: "Found no usable HandBrakeCLI";
}

#| Retrieve the HandBrakeCLI command-line, with basic parameters preconfigured
method !HDBCommand (*@args) returns Array
{
    my @cmd = self!HDBBaseCommand();
    @cmd.push('-i',$.device);
    if !$.try-dvdnav
    {
        @cmd.push('--no-dvdnav');
    }
    @cmd.append(@args);
    return @cmd;
}

#| Detects the HandBrakeCLI version, verifying --json support
method detectAndValidateHDBVersion ()
{
    if $!HDBVersionValidated
    {
        return;
    }
    $!HDBVersionValidated = True;
    my @baseCommand = self!HDBBaseCommand;
    my $info = self.getInputFrom([|self!HDBBaseCommand(),'--version'], :timeoutGuard(60));
    my $data = $info.lines;
    $data.chomp;
    $data ~~ s/:s^\D+//;
    my @version = $data.split(/\./);
    my $major = 0;
    my $minor = 0;
    try
    {
        CATCH
        {
            self.printd: "HandBrakeCLI is of an unknown version (git snapshot?).";
            my $helpInfo = self.getInputFrom([|self!HDBBaseCommand(),'--help'], :timeoutGuard(60));
            if $helpInfo ~~ /\-\-multi\-pass/
            {
                $!HDBUsesMultiPass = True;
            }
            if $helpInfo ~~ /\-\-json/
            {
                self.printd: "Verified that it supports json, continuing.";
                return;
            }
            else
            {
                self.errOut("Your version of HandBrakeCLI is too old. Please upgrade to version 1.1 or above");
            }
        }
        $major = @version[0].Int;
        $minor = @version[1].Int;
    }
    if ($major == 1 && $minor >= 1)
    {
        self.printd: "HandBrakeCLI is >= 1.1, supports --json";
        # --two-pass was renamed to --multi-pass in 1.7
        if ($minor >= 7)
        {
            $!HDBUsesMultiPass = True;
        }
    }
    elsif ($major > 1)
    {
        say "WARNING: Your HandBrakeCLI is very new ($major.$minor).\nlazybrake was written with 1.x in mind. Will continue,\nbut if backwards incompatible changes have been made,\nthis might break";

        $!HDBUsesMultiPass = True;
    }
    else
    {
        self.errOut("Your version of HandBrakeCLI is too old. Please upgrade to version 1.1 or above");
    }
}

#| Output debugging text
method printch(Str $message)
{
    self.printd('[Chapter hack engine] '~$message);
}

#| Post-processes chapters for the chapter hack
method postProcessChapterHackChapters (@chapters, $discTitle,$titleNo,$length,$hdbData) returns Array
{
    my @chapterResult;
    self.printch: "Chapter hack is possible for title "~$titleNo;
    $discTitle.chapterHackPossible = True;
    if $length > ( $discTitle.totalInSeconds + 120)
    {
        self.printch: "Chapter hack is recommended for title "~$titleNo~" due to size differences";
        $discTitle.chapterHackRecommended = True;
    }
    elsif $hdbData.<TitleList>.elems == 1 && $.tv && ($discTitle.totalInSeconds > $.max-duration)
    {
        self.printch: "Chapter hack is recommended for title "~$titleNo~" due to TV mode and a single title longer than --max-duration";
        $discTitle.chapterHackRecommended = True;
    }
    if $.chapterHackExpectedTitles > 0
    {
        my $chaptersPerTitle = @chapters.elems / $.chapterHackExpectedTitles;
        if $chaptersPerTitle !== $chaptersPerTitle.floor
        {
            self.printch("Dumping chapters:");
            for @chapters -> $chapter
            {
                self.printch(sprintf("%d: %-5d seconds [%02d:%02d:%02d]", $chapter.chapterNo, $chapter.totalInSeconds, $chapter.Hours, $chapter.Minutes, $chapter.Seconds));
            }
            if $discTitle.chapterHackRecommended
            {
                $chaptersPerTitle = $chaptersPerTitle.floor;
                say "WARNING: Chapter hack engine is unable to evenly distribute chapters to make "~$.chapterHackExpectedTitles~" titles. Will make the last file longer than the rest";
                self.printch("Unable to reliably process chapter hack for title "~$titleNo~" because we found "~@chapters.elems~" chapters, which does not fit into an expected title count of "~$.chapterHackExpectedTitles~" (results in "~$chaptersPerTitle~" chapters per title).");
            }
            else
            {
                self.printv: 'Title '~$titleNo~": does not fit into expected chapter hack length, skipping chapter hack for this title";
                $discTitle.chapterHackPossible = False;
                return;
            }
        }
        self.printch: "Will convert chapters into "~$.chapterHackExpectedTitles~" titles consisting of $chaptersPerTitle chapters each";
        my $iter = Inf;
        my $currChapter;
        for @chapters -> $chapter
        {
            $iter++;
            if ($iter > $chaptersPerTitle && @chapterResult.elems < $.chapterHackExpectedTitles)
            {
                $iter = 1;
                $currChapter = LazyBrake::DVDChapterSet.new;
                @chapterResult.push($currChapter);
            }
            $chapter.mergeWith($currChapter);
            $currChapter.chapters.push($chapter.chapterNo);
            self.printch("Remapped chapter "~$chapter.chapterNo~" to entry number "~@chapterResult.elems);
        }
    }
    else
    {
        @chapterResult = @chapters;
    }
    return @chapterResult;
}

#| Parses a HandBrakeCLI --scan --json output
method parseHDBScan (*@cmd) returns Hash
{
    self.detectAndValidateHDBVersion();

    my %data = (
        titleOrder => [],
        titles     => {}
        canRipEncrypted => True,
    );

    @cmd.push('--json');

    # Load HandBrakeCLI JSON data
    my $in = self.getInputFrom(@cmd,'out');
    my $jsonString = $in.lines.join("\n");
    if ($jsonString ~~ /Encrypted\s+DVD\s+support\s+unavailable/)
    {
        self.printd: "HandBrakeCLI can not rip encrypted discs";
        %data<canRipEncrypted> = False;
    }
    $jsonString ~~ s:g/^^libdvd(read|nav)\:\N+//;
    $jsonString ~~ s:g/^^
        (\w
            <-[:]> +
        )
        /"$0"/;
    $jsonString ~~ s:g/^^\}$$/\}\,/;
    $jsonString = '{'~$jsonString~'}';
    $jsonString ~~ s/\}\s*\,\s*\}/}}/;
    my $hdbData;
    try
    {
        CATCH
        {
            default
            {
                self.printd: "Failed to parse JSON: "~.message;
                self.printd: "Attempted to parse string: "~$jsonString;
            }
        }
        $hdbData = from-json($jsonString).{"JSON Title Set"};
    }
    if !$hdbData
    {
        return %data;
    }
    my %metadata = self.getDiscMetadata();

    if $hdbData.<MainFeature>
    {
        self.printd: "HandBrakeCLI says MainFeature is "~$hdbData.<MainFeature>;
    }

    for $hdbData.<TitleList>.list -> %title
    {
        my $titleNo = %title.<Index>;
        my @idComponents = (
            $hdbData.<MainFeature> == $titleNo ?? 'main' !! 'non-main',
            $hdbData.<InterlaceDetected> ?? 'interlaced' !! 'not-interlaced',
            %title.<Duration>.<Hours>,
            %title.<Duration>.<Minutes>,
            %title.<Duration>.<Seconds>,
            %title.<Duration>.<Ticks>,
            %title.<Geometry>.<Height>,
            %title.<Geometry>.<Width>,
            %title.<AngleCount>,
            %title.<Type>,
            %title.<VideoCodec>,
        );
        if %title<AudioList>.elems == 0 && %title<SubtitleList>.elems == 0
        {
            self.printd: "Skipping title $titleNo: no audio or subtitles present";
            next;
        }
        else
        {
            self.printd: "Identified title $titleNo";
        }
        %data{'titleOrder'}.push($titleNo);
        my $discTitle = LazyBrake::DiscTitle.new(
            :$titleNo,
            subtitles => {},
            audio => {},
            mainTitle => $hdbData.<MainFeature> == $titleNo,
            interlaced => !!$hdbData.<InterlaceDetected>,
            chapterHackPossible => False,
            chapterHackRecommended => False,
            Hours => %title.<Duration>.<Hours>,
            Minutes => %title.<Duration>.<Minutes>,
            Seconds => %title.<Duration>.<Seconds>,
        );
        %data{'titles'}.{$titleNo} = $discTitle;

        # Chapter hack engine (only enabled on actual discs, not on files)
        if %title<ChapterList> && !%metadata<FILE>
        {
            # Chapters are 1-indexed, not 0-indexed
            my $no = 0;
            my @chapters;
            my $length = 0;
            for %title<ChapterList>.list -> %chapter
            {
                $no++;
                if (%chapter<Name> ne 'Chapter '~$no)
                {
                    self.errOut: "Got unexpected chapter name: "~%chapter<Name>, :canRecover, :internalError;
                }
                my $totalInSeconds = (%chapter.<Duration>.<Seconds> + (%chapter.<Duration>.<Minutes>*60) + ((%chapter.<Duration>.<Hours>*60)*60));
                $length += $totalInSeconds;
                my $lengthRequired = 120;
                if $.chapterHackExpectedTitles
                {
                    $lengthRequired = 5;
                }
                if $totalInSeconds >= $lengthRequired
                {
                    @chapters.push(
                        LazyBrake::DVDChapter.new(
                            chapterNo => $no,
                            Seconds => %chapter.<Duration>.<Seconds>,
                            Minutes => %chapter.<Duration>.<Minutes>,
                            Hours => %chapter.<Duration>.<Hours>,
                        )
                    );
                }
                else
                {
                    self.printd: '[Chapter hack engine] Skipping chapter '~$no~' of title '~$titleNo~': shorter than '~$lengthRequired~' seconds ('~$totalInSeconds~')';
                }
            }
            if @chapters.elems > 1
            {
                $discTitle.chapters = self.postProcessChapterHackChapters(@chapters,$discTitle,$titleNo,$length,$hdbData);
            }
        }
        my $subID = 0;
        for %title<SubtitleList>.list -> %subtitle
        {
            $subID++;
            if self.addEntryToLanguageList(%data<titles>.{$titleNo}, $subID, %subtitle<LanguageCode>, %subtitle<Language>, 'subtitles')
            {
                @idComponents.push: $subID, %subtitle<LanguageCode>, %subtitle<Language>, %subtitle<Source>;
                self.printd: 'Identified subtitle '~%subtitle<LanguageCode>~' ('~%subtitle<Language>~') with ID '~$subID;
            }
        }
        my $audioID = 0;
        for %title<AudioList>.list -> %audioTrack
        {
            $audioID++;
            if self.addEntryToLanguageList(%data<titles>.{$titleNo}, $audioID, %audioTrack<LanguageCode>, %audioTrack<Language>,'audio')
            {
                @idComponents.push: $audioID, %audioTrack<LanguageCode>, %audioTrack<Language>, %audioTrack<Codec>, %audioTrack<BitRate>,%audioTrack<SampleRate>,%audioTrack<ChannelCount>;
                self.printd: 'Identified audio track '~%audioTrack<LanguageCode>~' ('~%audioTrack<Language>~') with ID '~$audioID;
            }
        }
        for %title<ChapterList>.list -> %chapter
        {
            @idComponents.push(%chapter<Name>);
            @idComponents.push(%chapter<Duration>.<Ticks>);
        }
        $discTitle.idString = @idComponents.join('-');
        self.printd: 'ID string for title '~$titleNo~': '~$discTitle.idString;
    }
    return %data;
}

#|{Appends scan data relating to languages to a hash
#|Deduplicating entries and renaming as needed}
method addEntryToLanguageList ($titleObj, Int $id, Str $langCode is copy, Str $langName, Str $type) returns Bool
{
    if $titleObj."$type"().{$langCode}:exists
    {
        if $type eq 'subtitles'
        {
            my $name = $langName;
            $name ~~ s:s/(Wide Screen|Letterbox)//;
            my $origName = $titleObj."$type"(){$langCode}<name>;
            $origName ~~ s:s/(Wide Screen|Letterbox)//;

            if $origName eq $name
            {
                self.printd: 'Detected Wide Screen/Letterbox versions of subtitles. Ignoring duplicates for '~$langCode;
                return False;
            }
        }
        $langCode = $langCode~'-'~$id;
    }
    $titleObj."$type"().{$langCode~$id} = {
        name => $langName,
        id => $id,
    };
    return True;
}

#| Performs a scan with HandBrakeCLI --scan and returns its data in a hash
method scan ($gentle = False) returns Hash
{
    my %dvdNavResult;
    my %result;
    my $success = False;
    my @cmd = self!HDBCommand('--min-duration',$.min-duration,'--scan','-t',0,'--previews=1:0');
    try
    {
        %result = self.parseHDBScan: @cmd;
        $success = True;
    }

    if !$.try-dvdnav || !%result<canRipEncrypted>
    {
        if !$success
        {
            if $gentle
            {
                return {
                    _failed => True
                };
            }
            if !%result<canRipEncrypted>
            {
                self.errOut: "Scanning failed, your HandBrake installation does not support ripping encrypted disks", canRecover => True;
            }
            else
            {
                self.errOut: "Scanning failed, try to enable dvdnav", canRecover => True;
            }
        }
        return %result;
    }

    my %metadata = self.getDiscMetadata();
    if !$success
    {
        say "error - use of dvdnav failed, trying without dvdnav";
    }
    elsif %result<titleOrder>.elems <= 1 && !%metadata<FILE>
    {
        say "questionable result - trying without dvdnav";
        self.printd: "Only got "~%result<titleOrder>.elems~" titles, not trusting the result, will retry without libdvdnav";
        $success = False;
        %dvdNavResult = %result;
    }

    if !$success
    {
        print "Scanning without dvdnav...";
        @cmd.push('--no-dvdnav');
        if $gentle
        {
            try
            {
                CATCH
                {
                    return {
                        _failed => True
                    };
                }
                %result = self.parseHDBScan: @cmd;
            }
        }
        else
        {
            %result = self.parseHDBScan: @cmd;
        }
        if %result<titleOrder>.elems == 0 && %dvdNavResult<titleOrder>.elems > 0
        {
            self.printd: "Result without dvdnav was unusable, falling back to dvdnav";
            %result = %dvdNavResult;
        }
        if %result<titleOrder>.elems == 0
        {
            self.printd: "Scanning failed, even without dvdnav. Going to re-scan with dvdnav but without a --min-duration";
            @cmd = self!HDBCommand('--scan','-t',0,'--previews=1:0');
            if $gentle
            {
                try
                {
                    CATCH
                    {
                        return {
                            _failed => True
                        };
                    }
                    %result = self.parseHDBScan: @cmd;
                }
            }
            else
            {
                %result = self.parseHDBScan: @cmd;
            }
        }
    }
    return %result;
}

#| Get an array of audio/subtitle params for HandBrakeCLI
method buildAudioSubParams(:$discTitle!, :$title!)
{
    my @params;
    my $type;
    my @skippedSubs;
    my @skippedAudio;

    for <subtitles audio> -> $type
    {
        my @final;
        my $entries;
        my $dataType;
        if ($type eq 'subtitles')
        {
            $entries = $.subtitle;
            $dataType = 'subtitles';
        }
        elsif ($type eq 'audio')
        {
            $entries = $.audiolang;
            $dataType = 'audio';
        }
        else
        {
            die;
        }

        if (!$entries)
        {
            next;
        }

        for $entries.split(',') -> $entry
        {
            my $origTitle = $entry;
            my $found = False;
            if $entry && $discTitle."$dataType"() && !$discTitle."$dataType"().{$entry}
            {
                for $discTitle."$dataType"().keys -> $subkey
                {
                    if $discTitle."$dataType"().{$subkey} && ($discTitle."$dataType"().{$subkey}.<name>:exists) && $discTitle."$dataType"().{$subkey}.<name> eq $entry
                    {
                        self.printv: "Remapped $entry to $subkey";
                        last;
                    }
                }
            }
            for $discTitle."$dataType"().keys.sort -> $checkEntry
            {
                my $include = False;
                if $.inclusive-lang
                {
                    if $checkEntry.starts-with($entry)
                    {
                        $include = True;
                    }
                }
                else
                {
                    if $entry eq $checkEntry
                    {
                        $include = True;
                    }
                }
                if $include
                {
                    $found = True;
                    self.printd: 'Using '~$discTitle."$dataType"().{$checkEntry}.<id>~' as '~$dataType~' ID for '~$origTitle;
                    @final.push($discTitle."$dataType"().{$checkEntry}.<id>);
                }
            }
            if !$found
            {
                if ($dataType eq 'subtitles')
                {
                    @skippedSubs.push($origTitle);
                }
                elsif ($dataType eq 'audio')
                {
                    @skippedAudio.push($origTitle);
                }
            }
        }
        if @final.elems > 0
        {
            if ($dataType eq 'subtitles')
            {
                @params.push('--subtitle',@final.join(','));
            }
            elsif ($dataType eq 'audio')
            {
                @params.push('--audio',@final.join(','));
            }
        }
    }
    if @skippedAudio.elems > 0
    {
        self.printv: "Skipped audio languages: "~@skippedAudio.join(', ');
        if $.audiolang.split(',').elems == @skippedAudio.elems
        {
            say "Warning: no audio languages matching your --audiolang found, ripping all audio tracks.";
            @params.push('--all-audio');
        }
    }
    if @skippedSubs.elems > 0
    {
        self.printv: "Skipped subtitles: "~@skippedSubs.join(',');
        if $.subtitle.split(',').elems == @skippedSubs.elems
        {
            if $discTitle.subtitles.elems > 0
            {
                say "Warning: none of the requested --subtitles were found, ripping all subtitles.";
                @params.push('--all-subtitles');
            }
            else
            {
                say "Warning: disc has no subtitles";
            }
        }
    }

    return @params;
}

#| Rips a single title using the settings provided
method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, :$chapter = Nil, Int :$jobNumber = 0, Int :$elementsToRip = 1, Bool :$no-dvdnav is copy = False, Int :$custom-timeout-guard) returns LazyBrake::RipStatus
{
    my @command = self!HDBCommand('-v0', '-t', $title, '-o', $filename,'--preset',$.hdb-preset,'--previews=1:0');
    @command.append: self.buildAudioSubParams(:$discTitle,:$title);
    my $success = False;
    my $validationDuration = $discTitle.totalInSeconds;
    if $.two-pass
    {
        self.detectAndValidateHDBVersion();
        if $!HDBUsesMultiPass
        {
            @command.append: '--multi-pass';
        }
        else
        {
            @command.append: '--two-pass';
        }
    }
    if $.auto-deinterlace
    {
        @command.push( '--comb-detect','--decomb' );
    }
    if $no-dvdnav && $.try-dvdnav
    {
        @command.push('--no-dvdnav');
    }
    if $chapter
    {
        @command.push('--chapters',$chapter.chapterNo);
        # Needs to use the chapter length rather than the title length in
        # chapterHack mode
        $validationDuration = $chapter.totalInSeconds;
    }
    # Apply the expected container format.
    # This is usually mp4, however we have an attribute that we get from ::Role
    # that specifies if we're being used as an encoder for blu-ray data, in
    # which case we try to auto-switch over to mkv to make sure that subtitles
    # get encoded properly (mp4 doesn't support the BD subtitle format). This
    # auto-switch is made by setting $.extension to mkv.
    if $.extension eq 'mkv'
    {
        @command.append: '--format','av_mkv';
        # Explicitly use surround
        @command.append: '--aencoder','av_aac';
    }
    else
    {
        @command.append: '--format','av_mp4';
    }
    # Apply film grain optimization
    if $.optimize-film-grain
    {
        my $nlmeans = $.nlmeans;
        if !$nlmeans.defined
        {
            $nlmeans = 'light';
        }
        @command.append: '--nlmeans='~$nlmeans;
    }
    my $timeoutGuard = $custom-timeout-guard // self.getTimeoutGuard();
    if self.runForegroundProcess(@command, :$timeoutGuard) == True
    {
        $success = True;
    }
    else
    {
        self.printd: "runForegroundProcess for "~@command.join(' ')~" returned false";
    }
    if !$no-dvdnav && (!$filename.IO.e || !self.validateFile(file => $filename, duration => $validationDuration, :$discTitle))
    {
        if !$.try-dvdnav
        {
            say "(error, trying to re-rip)";
        }
        else
        {
            say "(error, trying to re-rip without using dvdnav)";
            @command.push('--no-dvdnav');
        }
        sleep(2);
        my $tmpStorageName = self.saveBackupFile($filename);
        if self.runForegroundProcess(@command, :$timeoutGuard) == False
        {
            self.printd: "runForegroundProcess for "~@command.join(' ')~" returned False, returning unsuccessful RipStatus";
            return LazyBrake::RipStatus.new(:!success, :tmpName($tmpStorageName));
        }
        if (!$filename.IO.e)
        {
            self.printd: "ripped file did not exist, returning unsuccessful RipStatus";
            return LazyBrake::RipStatus.new(:!success, :tmpName($tmpStorageName));
        }
        if (!self.validateFile(file => $filename, duration => $validationDuration, :$discTitle))
        {
            self.printd: "rip validation failed, returning unsuccessful RipStatus";
            return LazyBrake::RipStatus.new(:!success, :tmpName($tmpStorageName));
        }
        self.printd: "Rip completed successfully, returning successful RipStatus";
        return LazyBrake::RipStatus.new(:success, :tmpName($tmpStorageName), :fileSize($filename.IO.s));
    }
    return LazyBrake::RipStatus.new(:$success, :fileSize($filename.IO.s));
}

# This can be overridden by subclasses to remap titles. Used, for instance, by
# the HDB::EncoderOnly backend so hour messages will refer to the title on the
# disc, instead of the title in the file (which is pretty much always 1)
method convertInternalTitleToRealTitle ($title)
{
    return $title;
}
