#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::DiscTitle;
#| Representation of a single chapter of a DVD
class LazyBrake::backend::makemkv::MakemkvTitle is LazyBrake::DiscTitle
{
    has Bool $.BD = False;
    has $.makemkvInternalTitle is required;
    has $.makemkvGuessPriority is rw;
    has $.parent;

    method makemkvInternalFilename (:%discInfo)
    {
        return 'title_t'~sprintf("%02d",$.makemkvInternalTitle)~'.mkv';
    }

    method compareFilenameWithLabel($label is copy,$filename is copy)
    {
        $label ~~ s:g/\W//;
        $label ~~ s:g/_//;
        $filename ~~ s:g/\W//;
        $filename ~~ s:g/_//;
        return $filename.lc.contains($label.lc);
    }

    method resolveFilename (%discInfo)
    {
        my $basicFilename = self.makemkvInternalFilename;
        if $basicFilename.IO.e
        {
            return $basicFilename;
        }
        my $label = %discInfo<LABEL> // 'unknown_label';
        my $labelFilename = $label~'_t'~sprintf("%02d",$.makemkvInternalTitle)~'.mkv';
        if $labelFilename.IO.e
        {
            return $labelFilename;
        }
        # Yes, this is very very hacky. Like, incredibly hacky.
        # On the positive side. It works.
        (my $spaceLabel = $label) ~~ s:g/_/ /;
        my $labelSpaceFilename = $spaceLabel~'_t'~sprintf("%02d",$.makemkvInternalTitle)~'.mkv';
        if $labelSpaceFilename.IO.e
        {
            return $labelSpaceFilename;
        }
        # If all of the others failed, then we do a case-insensitive search
        # comparing all files to find it, if THAT fails, then we check for *mkv
        # files matching _t\d.mkv and that contains $label, and if only a
        # single one matching that exists then we use that. Then we search for
        # files matching _t\d.mkv, if only a single *_t\d.mkv exists, we use
        # that. If that also fails, we check if there's only a single
        # *mkv, if there is, we use that. If there are multiple *mkv's we
        # use the latest one, as long as that is >=100M and has been modified
        # in the last 5 minutes.
        my @mkvFiles;
        my @mkvFilesMatched;
        my @mkvFilesLabelMatched;
        for dir() -> $entry
        {
            if $entry ~~ /:i \.mkv$/
            {
                @mkvFiles.push( $entry );
                if $entry ~~ /:i _t\d+\.mkv$/
                {
                    @mkvFilesMatched.push( $entry );
                    if self.compareFilenameWithLabel($label,$entry)
                    {
                        @mkvFilesLabelMatched.push( $entry );
                    }
                }
            }
            for ($basicFilename, $labelFilename, $labelSpaceFilename) -> $test
            {
                if $entry.lc eq $test.lc
                {
                    return $entry;
                }
            }
        }
        if @mkvFilesMatched.elems == 1
        {
            return @mkvFilesMatched[0];
        }
        if @mkvFilesLabelMatched.elems == 1
        {
            return @mkvFilesLabelMatched[0];
        }
        if @mkvFiles.elems == 1
        {
            return @mkvFiles[0];
        }
        # Pick the newest file if it has been modified in the last 5 minutes
        # and is >= 100MiB
        my $modifiedSince = DateTime.now-(60*5);
        my $newest = @mkvFiles.sort(-*.modified).first;
        if ( ($newest.modified > $modifiedSince ) && ($newest.s >= 104857600) )
        {
            return $newest;
        }
        self.printd: "mkvFilesMatched is "~@mkvFilesMatched.raku;
        self.printd: "mkvFilesLabelMatched is "~@mkvFilesLabelMatched.raku;
        self.printd: "mkvFiles is "~@mkvFiles.raku;
        die 'resolveFilename: failed to guess the makemkv output filename. This is either a deficiency in lazybrake or a problem with makemkv';
    }
}
