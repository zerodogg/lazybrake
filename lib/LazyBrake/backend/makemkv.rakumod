#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend;
#|{ Backend: makemkvcon
  Note: This is a hybrid backend. makemkvcon is used for ripping, but HandBrakeCLI
  still handles scanning
}
unit class LazyBrake::backend::makemkv does LazyBrake::backend;
use LazyBrake::backend::makemkv::MakemkvTitle;
use LazyBrake::RipStatus;
use File::Temp;
use File::Directory::Tree;

has Str $.extension = 'mkv';
has @.dependencies = ([ 'makemkvcon', 'makemkv.makemkvcon','flatpak' ],);

method _makemkvCommand ()
{
    if self.inPath('makemkvcon')
    {
        return 'makemkvcon';
    }
    elsif self.inPath('makemkv.makemkvcon')
    {
        return 'makemkv.makemkvcon';
    }
    elsif self.isFlatpakInstalled('com.makemkv.MakeMKV')
    {
        return self.getFlatpakCommand('com.makemkv.MakeMKV','makemkvcon');
    }
    die "::Backend::Makemkv used without any makemkv";
}

#| Verify that unsupported features are not enabled. Override this method
#| in your subclass if you implement the feature yourself.
method unsupportedFeatureCheck ()
{
    if $.subtitle || $.audiolang
    {
        say "Note: The makemkv backend combines --subtitle and --audiolang into one";
        say "      If you want specific subtitles or audio languages, use the makemkv-hdb";
        say "      backend."
    }
    if $.two-pass && self.^name eq 'backend_mmkv'
    {
        self.printv: 'Note: The makemkv backend does not support (or need) --two-pass';
    }
}

method filterQuotes ($on is copy)
{
    if $on
    {
        $on ~~ s:g/^\"(.*)\"$/$0/;
    }
    return $on;
}

method makemkvTempDir ()
{
    # Get the home dir
    my $HOME = $*HOME;
    if ! $HOME.IO.e
    {
        die "Unable to resolve home dir";
    }
    # Get the user's .MakeMKV
    my $MakeMKVSourceDir = $HOME~'/.MakeMKV';
    if ! ($MakeMKVSourceDir~'/settings.conf').IO.e
    {
        return Nil;
    }
    # Create our temporary directory
    my $tmp = tempdir(
        :prefix('lazybrake-makemkvhome'),
        :unlink
    );
    my $MakeMKVDir = $tmp~'/.MakeMKV';
    $MakeMKVDir.IO.mkdir;
    # Populate our temporary directory
    for $MakeMKVSourceDir.IO.dir() -> $entry
    {
        if $entry.IO.basename ne 'settings.conf'
        {
            $entry.IO.symlink($MakeMKVDir~'/'~$entry.IO.basename) || die;
        }
    }
    my @settingsConf;
    for ($MakeMKVSourceDir~'/settings.conf').IO.lines -> $line
    {
        given $line
        {
            when /^app_DefaultSelectionString/
            {
                # Skip
            }
            default
            {
                @settingsConf.push: $line;
            }
        }
    }
    # Build app_DefaultSelectionString
    my @languages;
    if $.subtitle
    {
        @languages.append: $.subtitle.split(',');
    }
    if $.audiolang
    {
        @languages.append: $.audiolang.split(',');
    }
    # Basic sanity check
    if @languages.grep(/\"/).elems > 0
    {
        die "Language list contains invalid character: \"";
    }
    # Based upon makemkv defaults
    my $app_DefaultSelectionString = '-sel:all,+sel:(favlang|nolang|'~@languages.unique.join('|')~'),-sel:mvcvideo,=100:all,-10:favlang';
    @settingsConf.push: "app_DefaultSelectionString = \""~$app_DefaultSelectionString~"\"";

    ($MakeMKVDir~'/settings.conf').IO.spurt: @settingsConf.join("\n");

    self.printd: "Created temporary home directory to override MakeMKV settings: "~$tmp;
    self.printd: "Generated app_DefaultSelectionString: "~$app_DefaultSelectionString;

    return $tmp;
}

method runMakemkvRip (@command, Int :$timeoutGuard = 0) returns LazyBrake::RipStatus
{
    my $stdout = 'pseudo-passthrough';
    if $.verbosity >= 199
    {
        $stdout = 'passthrough';
    }
    my $fakeTempHomeDir = self.makemkvTempDir();
    my $realHOME = %*ENV<HOME>;
    if $fakeTempHomeDir.defined && $fakeTempHomeDir.IO.e
    {
        %*ENV<HOME> = $fakeTempHomeDir;
        self.printd: "Overriding HOME for makemkv: "~$fakeTempHomeDir;
    }
    my $state;
    # Capture runSubProcess to make absolutely sure we reset %*ENV<HOME> once it finishes
    try
    {
        CATCH
        {
            default
            {
                self.prind: "Ignored exception from runSubProcess: "~.message;
            }
        }
        $state = self.runSubProcess(@command, :stderr<capture>, :$timeoutGuard, :$stdout);
    }
    if $fakeTempHomeDir.defined && $fakeTempHomeDir.IO.e && $fakeTempHomeDir.IO.d && $fakeTempHomeDir.IO.absolute ne $realHOME.IO.absolute
    {
        rmtree $fakeTempHomeDir;
        self.printd: "Removed "~$fakeTempHomeDir~" and reset HOME back to "~$realHOME;
        %*ENV<HOME> = $realHOME;
    }
    for $state.content.lines -> $line
    {
        given $line
        {
            when /:s UNRECOVERED READ ERROR/ | m{:s Posix error \- Input\/output error}
            {
                self.printd: "makemkv reported: "~$line.chomp;
                if $.verbosity >= 199
                {
                    self.printd: $state.content;
                }
                say "makemkv reports read errors during ripping. Broken disc?";
                return LazyBrake::RipStatus.new(:!success, :doNotRetry, :errorMessage<makemkv reports read errors during ripping. Broken disc?>);
            }
            when /:s ^Failed to save title/
            {
                self.printd: "makemkv reported: "~$line.chomp;
                say "makemkv failed to save the title";
                return LazyBrake::RipStatus.new(:!success, :errorMessage<makemkv failed to save the title>);
            }
        }
    }
    if !$state.success
    {
        return LazyBrake::RipStatus.new(:!success);
    }
    return LazyBrake::RipStatus.new(:success);
}

#| Rips a single title using the settings provided
method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, Int :$jobNumber = 0, Int :$elementsToRip = 1, :$preset = 'Normal', Bool :$no-dvdnav is copy = False) returns LazyBrake::RipStatus
{
    # First verify that no unsupported features have been requested
    self.unsupportedFeatureCheck();

    self.resetBaseBackendAttributes();

    my $internalTitle = $discTitle.makemkvInternalTitle;
    if $internalTitle.defined
    {
        self.printd: 'Using internal title '~$internalTitle~' to rip real title '~$title;
    }
    else
    {
        $internalTitle = $title;
    }

    my @command = (|self._makemkvCommand,'--noscan','--message=-stderr','--progress=-stdout','mkv','dev:'~$.device,$internalTitle,$*CWD);
    my $timeoutGuard = self.getTimeoutGuard();
    # BDs can take a lot longer to rip (and encode), so we increase the timeout
    # by a lot (5 hours)
    if $discTitle.BD && $.timeout-guard
    {
        $timeoutGuard = 18000;
    }
    print "makemkv: ";
    my $status = self.runMakemkvRip(@command, :$timeoutGuard);
    if !$status.success
    {
        return $status;
    }
    # makemkvcon doesn't let us specify the filename, so we rename it manually
    my $makemkvName = $discTitle.makemkvInternalFilename;
    if ! $makemkvName.IO.e
    {
        my %discMeta = self.getDiscMetadata();
        try
        {
            CATCH
            {
                default
                {
                    self.errOut: .message, :internalError, :canRecover;
                }
            }
            $makemkvName = $discTitle.resolveFilename(%discMeta);
            self.printd: "Guessed makemkv filename: "~$makemkvName;
        }
    }
    else
    {
        self.printd: "Found makemkv file with default name: "~$makemkvName;
    }

    if ! $makemkvName.IO.e
    {
        self.errOut: 'failed to guess the makemkv output filename. This is either a deficiency in lazybrake or a problem with makemkv', :internalError, :canRecover;
    }

    if $filename.IO.e
    {
        self.errOut: $filename~': already exists, unable to rename '~$makemkvName~' to it. You should manually move '~$makemkvName~' to '~$filename~' and then re-run lazybrake', canRecover => True;
    }
    $makemkvName.IO.rename($filename);
    return LazyBrake::RipStatus.new(:success, :fileSize($filename.IO.s));
}

#| Return the backends guessed title, or Nil. Implemented by some backends.
method getGuessedMainTitle (%scanData)
{
    my $bestGuess = Nil;
    my $bestGuessPriority = Nil;
    my $bestGuessLen = Nil;
    for %scanData<titles>.keys -> $titleID
    {
        my $mmkvTitle = %scanData<titles>{$titleID};
        my $len = $mmkvTitle.totalInSeconds;
        my $guessP = $mmkvTitle.makemkvGuessPriority;
        if $guessP.defined
        {
            if !$bestGuess.defined
            {
                $bestGuess = $titleID;
                $bestGuessPriority = $guessP;
                $bestGuessLen = $len;
            }
            # There can be more than one title with the same guess priority
            # from makemkv, in that case we pick the longest one.
            elsif $guessP == $bestGuessPriority
            {
                if $len > $bestGuessLen
                {
                    $bestGuess = $titleID;
                    $bestGuessPriority = $guessP;
                    $bestGuessLen = $len;
                }
            }
            elsif $guessP < $bestGuessPriority
            {
                $bestGuess = $titleID;
                $bestGuessPriority = $guessP;
                $bestGuessLen = $len;
            }
        }
    }
    if $bestGuess.defined
    {
        return $bestGuess.Int;
    }
    else
    {
        return Nil;
    }
}

#| Performs a scan with makemkvcon info
method scan () returns Hash
{
    my @cmd = (|self._makemkvCommand,'info','dev:'~$.device,'--robot');
    my %discMeta = self.getDiscMetadata();
    my $in = self.getInputFrom(@cmd,'out', :timeoutGuard(900));

    self.printd: "Got data from makemkvcon";

    # Keeping track of the previously seen message type, so that we can
    # reset sInfoState whenever the message type changes, and thus avoid
    # adding subtitles/audio tracks to the wrong title
    my $prev = 'NULL';
    # The results of the scan
    my %titles = (
        titleOrder => [],
        titles => {},
        canRipEncrypted => True,
    );
    # Fallback title IDs
    my $fallbackTitle = 50000;
    # The title that is currently being processed
    my $currTitle = -1;
    # The current type from SINFO
    my $sInfoState = 'NULL';
    # The results from a length message
    my $delayedLength;
    # Guessed ID for audio/sub language. makemkvcon won't use this, but in a
    # hybrid makemkvcon/HandBrakeCLI backend it can be used to try to rip
    # the correct language/subtitle.
    my $guessedTrackID;
    # Used to keep track of roundtrips through the loop where no data was parsed
    # so that we can time out if it takes too long (defined as 20 seconds of inactivity).
    my $noReplyNo = 0;
    for $in.lines -> $line
    {
        $noReplyNo = 0;
        # Retrieve the entry type (which is the first part of a line)
        my ($type,$opts) = ($line.split(':').first,$line);
        # Remove the type to get an opts line
        $opts ~~ s/^<[^:]>+://;
        # Parse out the various components of the current line
        my ($code,$flags,$count,$message,$data) = $opts.split(',');
        my $id = $code;
        $id ~~ s:g/:s\D//;
        $id = $id.Int;
        # MSG is a generic message. We only use it to check for evaluation version expiry.
        if $type eq 'MSG'
        {
            if $id == 5055
            {
                $*ERR.print("makemkv has expired, and is unusable\n");
                self.errOut: "makemkv has expired, and is unusable", canRecover => True,
            }
            elsif $id == 2004 && $message.contains('KEY EXCHANGE FAILURE - KEY NOT ESTABLISHED')
            {
                if @cmd[0].contains('flatpak')
                {
                    self.errOut: "makemkv reports that the drive refuses to read the disc. You may want to try with a makemkv package not installed through flatpak. Reported error: "~$message, canRecover => True,
                }
                else
                {
                    self.errOut: "makemkv reports that the drive refuses to read the disc: "~$message, canRecover => True,
                }
            }
            elsif $message.contains('This application version is too old.')
            {
                self.errOut: "makemkv needs to be updated, it reports that the application version is too old and refuses to work (ID=$id)", canRecover => True,
            }
            next;
        }
        # If the previous line type doesn't match the current type,
        # and the title is -1 (ie. no title yet), reset sInfoState.
        if $prev ne $type && $currTitle != -1
        {
            $sInfoState = 'NULL';
        }
        # Store the previous type
        $prev = $type;
        # Filter away quotes from message and data, if we have them
        $message = self.filterQuotes($message);
        $data = self.filterQuotes($data);
        # TINFO provides basic information about a title. We use it to
        # fetch the title number and then length of said title
        if $type eq 'TINFO'
        {
            # Flag 24 (DVD) or 16 (BD) indicates that the line is
            # identifying the title number
            if ($flags == 24 || $flags == 16)
            {
                $message = self.filterQuotes($message);
                $currTitle = $message;
                $currTitle ~~ s:g/:s\D+//;
                $currTitle = $currTitle.Int;
                my %timestamp;
                if ($delayedLength)
                {
                    %timestamp = self.parseTimestamp($delayedLength);
                }
                else
                {
                    self.errOut: "Title ($id/$currTitle) without length, giving up", :internalError, :canRecover;
                }
                my $internalTitle = $id;
                if %titles<titles>{$currTitle}.defined
                {
                    if %titles<titles>{$id}.defined && $internalTitle > 0 && $internalTitle < 50000
                    {
                        $fallbackTitle++;
                        self.printd: "There is already a title with id $currTitle. Using lazybrake title ID $fallbackTitle instead (its real title is $internalTitle)";
                        $currTitle = $fallbackTitle;
                    }
                    else
                    {
                        self.printd: "There is already a title with id $currTitle. Switching to using the internal makemkv title number ($id) instead";
                        $currTitle = $id;
                        if %titles<titles>{$id}.defined && $id == 1
                        {
                            %titles<titles><0> = %titles<titles><1>;
                            %titles<titleOrder>.unshift(0);
                            %titles<titleOrder>.pop();
                            self.printd: "There is already a title with the internal ID of 1. Renaming 1 to 0.";
                        }
                    }
                }
                self.printd: 'Identified title '~$currTitle~' (makemkv internal ID: '~$internalTitle~')';
                %titles<titles>{$currTitle} = LazyBrake::backend::makemkv::MakemkvTitle.new(
                    titleNo => $currTitle,
                    makemkvInternalTitle => $internalTitle,
                    Hours => %timestamp<hours>,
                    Minutes => %timestamp<minutes>,
                    Seconds => %timestamp<seconds>,
                    BD => %discMeta<BD>,
                    owner => self,
                );
                %titles<titleOrder>.push($currTitle);
            }
            # Flag 9 indicates that the line is providing the length of
            # the title
            elsif $flags == 9
            {
                $delayedLength = $message;
            }
            # Flag 49 is a comment from MakeMKV. Used, among other things, to
            # provide us with main title guesses. We use this over our own guess
            # method if it is present
            elsif $flags == 49 && $message ~~ /^T/
            {
                my $makemkvPriority = $message;
                $makemkvPriority ~~ s:g/:s\D//;
                if $makemkvPriority.chars > 0 && $makemkvPriority ~~ /\d/
                {
                    self.printd: 'Makemkv provided a guess priority: '~$makemkvPriority~' ('~$message~')';
                    %titles<titles>{$currTitle}.makemkvGuessPriority = $makemkvPriority.Int;
                }
            }
        }
        # SINFO provides additional info for the title, we use it to
        # retrieve the audio and subtitle languages for the most
        # recently seen title.
        elsif $type eq 'SINFO'
        {
            # The presence of a 6202 message indicates that it's an
            # audio message
            if $message eq '6202' && $sInfoState ne 'audio'
            {
                $sInfoState = 'audio';
                $guessedTrackID = 0;
            }
            # The presence of a 6203 message indicates that it's a
            # subtitle message
            elsif $message eq '6203' && $sInfoState ne 'subtitles'
            {
                $sInfoState = 'subtitles';
                $guessedTrackID = 0;
            }
            # count=3 indicates that this single line contains the ISO
            # language identifier for this $sInfoState entry
            if $count eq '3'
            {
                if $sInfoState eq 'NULL'
                {
                    self.printd: 'Got SINFO data, but sInfoState is null. Discarding it.';
                }
                else
                {
                    self.printd: "Identified "~$sInfoState~" track \""~$data~"\" for title "~$currTitle~" is track number "~$guessedTrackID;
                    $guessedTrackID++;
                    %titles<titles>{$currTitle}."$sInfoState"(){$data~$guessedTrackID} = {
                        name => $data,
                        id   => $guessedTrackID,
                    };
                }
            }
        }
    }
    self.printd('Finished scanning');
    return %titles;
}
