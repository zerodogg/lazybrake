#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend;
#| Scan-only backend: lsdvd
unit class LazyBrake::backend::ScanLsdvd does LazyBrake::backend;

#| This backend is NEVER directly usable
method usable () returns Bool
{
    return False;
}

#| Performs a scan with lsdvd
method scan () returns Hash
{
    # -Oh = enable "human" output (which is fairly parseable)
    # -a = list audio streams
    # -s = list subtitles
    my @cmd = ('lsdvd','-Oh','-a','-s',$.device);
    my $in = self.getInputFrom(@cmd);
    my $currentTitle = -1;
    my $currentSubtype;
    my %data = (
        titleOrder => [],
        titles     => {}
    );
    # Used to keep track of roundtrips through the loop where no data was parsed
    # so that we can time out if it takes too long (defined as 20 seconds of inactivity).
    my $noReplyNo = 0;
    while $in.exitcode == -1 || ($currentTitle == -1 && $noReplyNo < 200)
    {
        $noReplyNo++;
        if ($noReplyNo % 10) == 0
        {
            self.printd: "No data for $noReplyNo loops. Waiting one second.";
            sleep 1;
        }
        for $in.lines -> $line
        {
            $noReplyNo = 0;
            if $line ~~ /:s^Title\:/
            {
                my $title = $line;
                my $length = $line;

                $title ~~ s/:s^Title\:\s+0*(\d+)\D.*/$0/;
                $length ~~ s/:s^.*Length\:\s+(\S+)\s+.*/$0/;
                $currentTitle = $title;
                %data<titles>.{$title} = {
                    subtitles => {},
                    mainTitle => False,
                    duration => self.parseTimestamp($length),
                };
                %data<titleOrder>.push($title);
            }
            elsif $currentTitle == -1
            {
                next;
            }
            elsif $line ~~ /^\s+(Audio|Subtitle):/
            {
                my $saveTo;
                my $type = $line;
                my $id   = $line;
                my $lang = $line;

                $type ~~ s/:s^\s+(Audio|Subtitle).*/$0/;
                $id   ~~ s/:s^\s+(Audio|Subtitle)\:\s+0*(\d+)\,?\s+.*/$0/;
                $lang ~~ s/:s^.*Language\:\s+(\S+)\s.*/$0/;

                $saveTo = $type.lc;
                if $saveTo eq 'subtitle'
                {
                    $saveTo = 'subtitles';
                }
                # We need to check if it is already defined. If it is, then we
                # already have an entry for this language, and we'll assume that one
                # is the correct one.
                if !defined %data<titles>.{$currentTitle}.{$saveTo}.{$lang}
                {
                    %data<titles>.{$currentTitle}.{$saveTo}.{$lang} = {
                        name => 'FIXME',
                        id => $id,
                    };
                }
            }
            else
            {
                $line.chomp;
                if $line !~~ /^Longest\s*track/
                {
                    self.printd: "Unrecognized line from lsdvd (ignored): "~$line;
                }
            }
        }
    }
    return %data;
}

method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, Int :$jobNumber = 0, :$chapter = Nil, Int :$elementsToRip = 1, Bool :$no-dvdnav is copy = False)
{
    die "ScanLsdvd can't rip";
}

method extension ()
{
    die "ScanLsdvd can't rip";
}
