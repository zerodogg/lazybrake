#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend::HDB;

#|{Backend: (smarter) HandBrakeCLI
   Note: This is a variant of the normal backend_HDB, but with an override in
   place for the scan() method, so that lsdvd will be attempted before failing
   a scan.
}
unit class LazyBrake::backend::HDB::Smart is LazyBrake::backend::HDB;
use LazyBrake::backend::ScanLsdvd;

method scan()
{
    state $lsdvbackend;
    my %result = callwith(True);
    if %result<_failed>:exists
    {
        if !$lsdvbackend
        {
            $lsdvbackend = self.instantiateBackend(LazyBrake::Backend::ScanLsdvd);
        }
        print "Scanning using lsdvd...";
        return $lsdvbackend.scan();
    }
    return %result;
}
