#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI et.al. that streamlines
# and automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#| Role providing a method for parsing .hdb-preset*
unit role LazyBrake::backend::HDB::Role;

method getHDBPresetInfo ()
{
    my %discMeta = self.getDiscMetadata();
    my $hdbPreset = $.hdb-preset;
    if $.bdEncoder && $.hdb-preset-bd.defined
    {
        $hdbPreset = $.hdb-preset-bd;
    }
    my ($preset, $ext) = $hdbPreset.split('=');
    if !$ext.defined
    {
        if %discMeta<BD> || $.bdEncoder
        {
            $ext = 'mkv';
        }
        else
        {
            $ext = 'mp4';
        }
    }
    return { :$preset, :$ext };
}
