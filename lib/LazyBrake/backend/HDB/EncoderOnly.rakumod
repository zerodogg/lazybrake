#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI et.al. that streamlines
# and automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend::HDB;
unit class LazyBrake::backend::HDB::EncoderOnly is LazyBrake::backend::HDB;

# This can be used to specify a "real" title, so that when we output messages,
# for instance about missing languages, we output the title that we originally
# ripped, not the title internally to the file (which will pretty much always
# be 1).
has $.realTitle is rw;
# We need to be able to override device
has Str $.device is rw;

method convertInternalTitleToRealTitle ($title)
{
    if $.realTitle.defined
    {
        return $.realTitle;
    }
    return $title;
}
