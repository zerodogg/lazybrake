#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend::makemkv;
use LazyBrake::backend::HDB::EncoderOnly;
use LazyBrake::RipStatus;
use LazyBrake::backend::HDB::Role;
#| Backend: A makemkv variant that uses HDB to encode
unit class LazyBrake::backend::Hybrid::makemkvHDBEncode is LazyBrake::backend::makemkv does LazyBrake::backend::HDB::Role;

has @.dependencies = (['HandBrakeCLI','flatpak'],[ 'makemkvcon','makemkv.makemkvcon', 'flatpak' ]);
has Str  $.device is rw;
has $.hdbExtensionCache is rw;
has Bool $.shownLanguageMessage is rw = False;

method extension ()
{
    if ! $.hdbExtensionCache.defined
    {
        my $ext = self.getHDBPresetInfo.<ext>;
        $.hdbExtensionCache = $ext;
    }
    return $.hdbExtensionCache;
}

#| Verify that unsupported features are not enabled.
method unsupportedFeatureCheck ()
{
    # No unsupported features in this variant
}

#| Rips a single title using the settings provided, downscaling as needed
method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, Int :$jobNumber = 0, :$preset = 'Normal', Int :$elementsToRip = 1, Bool :$no-dvdnav is copy = False) returns LazyBrake::RipStatus
{
    $.hdbExtensionCache = Nil;

    # Fetch metadata so that we can instantiate the encoder in BD mode if
    # needed
    my %discMeta = self.getDiscMetadata();
    if %discMeta<BD>
    {
        $.bdEncoder = True;
    }
    else
    {
        $.bdEncoder = False;
    }
    my $mmkvFile = $filename~'-lazybrake-makemkv-rawfile~';
    my $return;

    if $mmkvFile.IO.e && $mmkvFile.IO.s > 9_663_676_416
    {
        say "Found an existing rip at "~$mmkvFile~" - assuming this is a complete dump.";
        say "You should doublecheck the resulting file to make sure it's not only a";
        say "partial rip.";
        $return = LazyBrake::RipStatus.new(:success, :fileSize($mmkvFile.IO.s));
    }
    else
    {
        # Hand control over to ripSingleTitle in backend_mmkv
        $return = callsame;
        if !$return.success
        {
            $.hdbExtensionCache = Nil;
            return $return;
        }
        $filename.IO.rename($mmkvFile);
    }
    $return = self.performHDBrip(:$title,:$filename,:$discTitle,:$preset,:$elementsToRip,:$no-dvdnav,:$return,:$mmkvFile,:$jobNumber,:%discMeta);
    # Attempt to re-encode files over 8,6 GiB and with a bitrate of >1.5MiB/s
    # with nlmeans filter if --auto-optimize-film-grain is on.
    if $.auto-optimize-film-grain && $return.fileSize > 9_223_372_800 && ! $.optimize-film-grain && self.getBitrateOfFileInMB($filename) > 1.50
    {
        say "Resulting file is very large ("~self.bytesToHumanReadable($return.fileSize)~"), attempting to";
        say "re-encode with optimize-film-grain.";
        my $oldSize = $return.fileSize;
        my $tmp = $filename~'.huge.tmp.~';
        $filename.IO.rename($tmp);
        $return = self.performHDBrip(:$title,:$filename,:$discTitle,:$preset,:$elementsToRip,:$no-dvdnav,:$return,:$mmkvFile,:$jobNumber,:%discMeta, :force-nlmeans);
        if ($tmp.IO.s * 0.8) > $filename.IO.s
        {
            say "Choosing the one with nlmeans ("~self.bytesToHumanReadable($tmp.IO.s)~' vs. '~self.bytesToHumanReadable($filename.IO.s)~')';
            $tmp.IO.unlink;
        }
        else
        {
            say "Filesize gain from re-encode not large enough, keeping the one without nlmeans ("~self.bytesToHumanReadable($tmp.IO.s)~' vs. '~self.bytesToHumanReadable($filename.IO.s)~')';
            $filename.IO.unlink;
            $tmp.IO.rename($filename);
            $return.fileSize = $filename.IO.s;
        }
    }
    $mmkvFile.IO.unlink;
    $.hdbExtensionCache = Nil;
    return $return;
}

method getBitrateOfFileInMB(Str $file)
{
    my %mediainfo = self.getMediaInfo($file);

    my $size = $file.IO.s;

    my $bitrate = $size/%mediainfo<video>;

    return $bitrate / 1_048_576;
}

method performHDBrip(Int :$title!, :$filename!, :$discTitle!, :$preset = 'Normal', Int :$jobNumber = 1, Int :$elementsToRip, Bool :$no-dvdnav is copy, :$return, :$mmkvFile,:%discMeta, Bool :$force-nlmeans = False) returns LazyBrake::RipStatus
{
    self.printd: 'Instantiating HDB child backend where device='~$mmkvFile;
    my $hdb = self.instantiateBackend(LazyBrake::backend::HDB::EncoderOnly, {
            :bdEncoder(%discMeta<BD>)
        });
    $hdb.device = $mmkvFile;
    $hdb.realTitle = $title;
    my %fileMetadata = $hdb.getDiscMetadata();
    if $force-nlmeans
    {
        $hdb.optimize-film-grain = True;
    }
    self.printd('Using HDB to scan the file');
    my %scanData = $hdb.scan();
    my @titles;
    if %scanData<titleOrder>.elems == 0
    {
        self.errOut: "Found no titles on makemkv file.", canRecover => True;
    }
    if @titles.elems == 0
    {
        self.printd: 'Resolving titles in the makemkv rip';
        try
        {
            CATCH
            {
                default
                {
                    self.printd( "Caught error during resolve: "~$_);
                }
            }
            @titles = $hdb.resolveTitles(%scanData);
        }
    }
    if @titles.elems == 0
    {
        self.errOut: "Found no titles in the makemkv ripped file.", canRecover => True;
    }
    if ($elementsToRip == 1 || $elementsToRip == $jobNumber) && $return.hasEjected != True
    {
        self.printd: "Ejecting before handing control over to HDB";
        if self.eject(:tolerate-failure)
        {
            $return.hasEjected = True;
        }
    }
    self.printd: 'Handing control over to HDB-backend to encode file';
    # BDs can take a lot longer to encode, so we increase the timeout for
    # HandBrakeCLI by a lot (5 hours)
    my $timeoutGuard = self.getTimeoutGuard();
    if %discMeta<BD> && $timeoutGuard > 0
    {
        $timeoutGuard = 18000;
    }
    my $fileSize = 0;
    for @titles -> $hdbSubTitle
    {
        my $discTitle = %scanData<titles>.{$hdbSubTitle},
        my $response = $hdb.ripSingleTitle(
            title => $discTitle.titleNo,
            :$filename,
            discTitle => $discTitle,
            :custom-timeout-guard($timeoutGuard),
            :$jobNumber,
        );
        if ! $filename.IO.e
        {
            self.errOut: 'Failed to encode makemkv rip ('~$filename~' did not exist after encoding finished)!', canRecover => True, couldBeBug => True;
        }
        if !$response.success
        {
            self.errOut: 'Failed to encode makemkv rip to '~$filename~' (response from encoder indicated failure)!', canRecover => True, couldBeBug => True;
        }
        $fileSize += $filename.IO.s;
    }
    $return.fileSize = $fileSize;
    return $return;
}
