#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend::HDB;
#| Backend: Hybrid between HandBrakeCLI and lsdvd, using HandBrakeCLI for ripping, lsdvd for scanning
unit class LazyBrake::backend::Hybrid::lsdvdscan is LazyBrake::backend::HDB;
use LazyBrake::backend::ScanLsdvd;
has Str @.dependencies = ('HandBrakeCLI','mediainfo','lsdvd');

method scan ()
{
    state $lsdvbackend;
    if !$lsdvbackend
    {
        $lsdvbackend = self.instantiateBackend(LazyBrake::backend::ScanLsdvd);
    }
    return $lsdvbackend.scan();
}
