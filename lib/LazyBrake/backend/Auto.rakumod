#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::backend;

#| Backend: Auto
unit class LazyBrake::backend::Auto does LazyBrake::backend;
use LazyBrake::backend::HDB;
use LazyBrake::backend::Hybrid::makemkvHDBEncode;
use LazyBrake::RipStatus;

has LazyBrake::backend $!currentBackend;
has Str $!currentBackendType;
has Str $!currentDiscType;
# This is used to count the number of times we have instantiated a backend
# since the last time we iterated through all backends in the default order.
# For instance, say we're ripping a TV series, and it needs makemkv. Then we
# don't want to try HandBrakeCLI on each disc and have it fail. So, we cache
# that we should try mamkemkv first. But there's still a chance that we don't
# need makemkv on subsequent discs, for instance when changing seasons. So, we
# compromise. We will ignore the previously used backend on every three attempts,
# in case we've cached something we no longer need. We also attempt all the other
# backends if the cached backend fails.
has Int $!attemptsSinceFullBackendCheck = 0;
has Bool $.containErrors = True;

method !measureBackendQuality (%scanData)
{
    my Int $quality = 0;
    my Int $maxQuality = -1;
    if $.tv
    {
        $maxQuality = 4;
        # If we had zero elements, then the scan returned nothing, so quality
        # should be 0
        if %scanData<titleOrder>.elems > 0
        {
            # Grant one point for having found something
            $quality++;
            # Penalize too many titles
            if %scanData<titleOrder>.elems >= 15
            {
                $quality--;
            }

            # Should not recommend the chapter hack
            my $chapterHackRecommended = False;
            for %scanData<titleOrder> -> $titleNo
            {
                my $entry = %scanData<titles>.{$titleNo};
                if $entry.<chapterHackRecommended>
                {
                    $chapterHackRecommended = True;
                }
            }
            if ! $chapterHackRecommended
            {
                $quality++;
            }

            my @resolved;
            try
            {
                @resolved = self.resolveTitles(%scanData, :silent);
            }
            # Grant points for 4+ resolved titles in series mode
            if @resolved.elems >= 4
            {
                $quality++;
            }
            # Grant points for having at most 10 resolved titles in series mode
            if @resolved.elems <= 10
            {
                $quality++;
            }
            # Penalize "no resolved titles"
            if @resolved.elems == 0
            {
                $quality--;
            }
            # Greatly penalize lack of support for encrypted disks
            if !%scanData<canRipEncrypted>
            {
                $quality -= 2;
            }
        }
    }
    elsif $.movie
    {
        $maxQuality = 3;
        # If we had zero elements, then the scan returned nothing, so quality
        # should be 0
        if %scanData<titleOrder>.elems > 0
        {
            # Grant one point for having found something
            $quality++;
            # Grant one point if we found fewer than 10 titles
            if %scanData<titleOrder>.elems < 10
            {
                $quality++;
            }
            my @resolved;
            try
            {
                @resolved = self.resolveTitles(%scanData, :silent);
            }
            # Grant one point if we had a main title
            if @resolved.elems == 1
            {
                $quality++;
            }
            # Greatly penalize lack of support for encrypted disks
            if !%scanData<canRipEncrypted>
            {
                $quality -= 2;
            }
        }
    }
    else
    {
        self.errOut: "Not in movie nor TV mode?", :internalError;
    }
    return {
        :$quality,
        :$maxQuality
    };
}

method !autoSelectBackend ()
{
    my %meta = self.getDiscMetadata();
    my $discType;
    my @attempt;
    if %meta<DVD>
    {
        $discType = 'DVD';
        @attempt = (
            LazyBrake::backend::Hybrid::makemkvHDBEncode,
            LazyBrake::backend::HDB,
        );
    }
    elsif %meta<BD>
    {
        $discType = 'BD';
        @attempt = (
            LazyBrake::backend::Hybrid::makemkvHDBEncode
        );
    }
    elsif %meta<FILE>
    {
        $discType = 'FILE';
        @attempt = (
            LazyBrake::backend::HDB
        );
    }
    else
    {
        self.errOut: "Unhandled source, not of type DVD, BD or FILE: "~%meta.raku, :internalError;
    }
    if $!currentDiscType.defined && $!currentDiscType eq $discType && $!attemptsSinceFullBackendCheck < 3
    {
        @attempt.unshift($!currentBackend);
        $!attemptsSinceFullBackendCheck++;
    }
    my $current;
    my $currentType;
    my %currentQuality = (
        :quality(0)
    );
    my %scanData;
    my %tried;
    my @exceptions;
    for @attempt -> $tryBackend
    {
        if %tried{ $tryBackend.^name }
        {
            next;
        }
        self.printd("Attempting backend: "~$tryBackend.^name);
        my $instance = self.instantiateBackend($tryBackend, {
            :bdEncoder(%meta<BD>)
        });
        %tried{ $tryBackend.^name } = True;
        if $instance.usable()
        {
            try
            {
                CATCH
                {
                    my $error = $tryBackend.^name~': threw exception during scan(): '~$_.message;
                    self.printd($error);
                    @exceptions.push($error);
                    next;
                }
                my %data = $instance.scan();
                my %quality = self!measureBackendQuality(%data);
                self.printd($tryBackend.^name~' quality: '~%quality<quality>);
                if %quality<quality> > %currentQuality<quality>
                {
                    $current = $instance;
                    $currentType = $tryBackend.^name;
                    %currentQuality = %quality;
                    %scanData = %data;
                }
                if %quality<quality> == %quality<maxQuality>
                {
                    self.printd("Accepting "~$tryBackend.^name);
                    last;
                }
            }
        }
        else
        {
            self.printd("Backend self-reports as unusable");
        }
        self.printnv: ".";
    }
    if $current.defined
    {
        self.printd("Using backend "~$current.^name);
        if $!currentBackend.defined && $!currentBackendType ne $currentType
        {
            $!attemptsSinceFullBackendCheck = 0;
        }
        $!currentBackend = $current;
        $!currentBackendType = $currentType;
        $!currentDiscType = $discType;
        return %scanData;
    }
    else
    {
        if @exceptions.elems > 0
        {
            self.errOut: "Unable to find a suitable backend. The following exceptions occurred while trying:\n"~@exceptions.join("\n"), canRecover => True;
        }
        else
        {
            self.errOut: "Unable to find a suitable backend", canRecover => True;
        }
    }
}

method usable () returns Bool
{
    if self.inPath('HandBrakeCLI') || self.isFlatpakInstalled('fr.handbrake.ghb')
    {
        if self.inPath('mediainfo')
        {
            return True;
        }
    }
    return False;
}

method scan () returns Hash
{
    return self!autoSelectBackend();
}

method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, Int :$jobNumber = 0, :$chapter = Nil, Int :$elementsToRip = 1, Bool :$no-dvdnav is copy = False) returns LazyBrake::RipStatus
{
    return $!currentBackend.ripSingleTitle(
        :$title,
        :$filename,
        :$discTitle,
        :$chapter,
        :$no-dvdnav,
        :$elementsToRip,
        :$jobNumber,
    );
}

method chosenBackend () returns Str
{
    return $!currentBackend.^name;
}

method extension ()
{
    return $!currentBackend.extension;
}
