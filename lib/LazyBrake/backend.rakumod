#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::base;
#| Base class for backends
unit role LazyBrake::backend is LazyBrake::base;

# This is a boolean that ::Auto and ::Hybrid::makemkvHDBEncode sets. It is used
# by ::HDB, but ignored by others.
has Bool $.bdEncoder is rw = False;
# Used to indicate if resolveTitles has guessed the title
has Bool $.guessedMainTitle is rw = False;
# Used to indicate if the guessed title is likely to be wrong
has Bool $.likelyWrongGuess is rw = False;
# Used to indicate if the guessed title is likely reliable
has Bool $.likelyReliableGuess is rw = False;

use JSON::Fast;

#| Returns a flatpak command if it is installed
method getFlatpakCommand(Str $flatpak, Str $command)
{
    if !self.inPath('flatpak')
    {
        return;
    }
    # Returns the --parameter (--user/--system) needed to find the flatpak, or nothing.
    my $source = self.isFlatpakInstalled($flatpak);
    if !$source.defined
    {
        return;
    }
    my @command = ('flatpak','run','--device=all','--no-documents-portal',$source,'--command='~$command,$flatpak);
    self.printd: "Generated flatpak command: "~@command.join(' ');
    return @command;
}

#| Checks if a flatpak is installed, and returns the parameter for where (--system/--user) if it is
method isFlatpakInstalled(Str $flatpak)
{
    if !self.inPath('flatpak')
    {
        return;
    }
    state $user = self.getInputFrom(['flatpak','list','--user'],'both');
    state $system = self.getInputFrom(['flatpak','list','--system'],'both');
    if $user.contains($flatpak)
    {
        self.printd: "Flatpak $flatpak is installed as --user";
        return '--user';
    }
    elsif $system.contains($flatpak)
    {
        self.printd: "Flatpak $flatpak is installed as --system";
        return '--system';
    }
    self.printd: "Flatpak $flatpak not installed";
    return;
}

#| Returns metadata about a media file using 'mediainfo'
method getMediaInfo (Str $file) returns Hash
{
    my %info = ();
    my $in = self.getInputFrom(['mediainfo','--output=JSON',$file],'both');
    my $jsonString = $in.lines.join("\n");

    my %mediaInfoData;
    try
    {
        CATCH
        {
            default
            {
                self.printd: "Failed to parse JSON: "~.message;
                return;
            }
        }
        %mediaInfoData = from-json($jsonString).hash;
    }

    for %mediaInfoData.{"media"}.{"track"}.list -> $entry
    {
        my $type = $entry.{"@type"}.lc;
        if $type eq 'video' || $type eq 'audio' || $type eq 'general'
        {
            %info{$type} = $entry.<Duration>.Numeric;
        }
        if $type eq 'video'
        {
            if %mediaInfoData<FrameRate_Minimum>:exists
            {
                %info<FrameRate_Minimum> = %mediaInfoData.<FrameRate_Minimum>.Numeric
            }
        }
    }
    return %info;
}

#| Validate that a file was properly ripped by looking at the file size and video length
method validateFile (Str :$file!, Rat :$largestRipped = 0.0, Int :$duration!, :$discTitle) returns Bool
{
    if ! $file.IO.e
    {
        self.printv: "$file: failed validation due to non-existant file";
        return False;
    }
    # Return true blindly if "no validation" was requested.
    # We still perform the most basic validation, ie. "does the file exist"
    # above, but nothing more.
    if !$.validateFiles
    {
        self.printd: 'Skipping file validation as requested';
        return True;
    }
    self.printd: 'Validating '~$file~' (size: '~$file.IO.s~')';

    my %fileMeta;
    if self.inPath('mediainfo')
    {
        %fileMeta = self.getMediaInfo($file);

        if %fileMeta<video>:!exists
        {
            self.printd: "No video element returned from getMediaInfo";
            self.printv: $file~": failed, the video track appears to be missing";
            return False;
        }
        if %fileMeta<audio>:!exists
        {
            self.printd: "No audio element returned from getMediaInfo";
            self.printv: $file~": failed, the audio track appears to be missing";
            return False;
        }
    }
    else
    {
        self.printd: "mediainfo not available, validation will be unreliable";
    }

    if %fileMeta<video>:exists
    {
        my $maxDuration = $duration*1.005;
        my $minDuration = $duration*0.994;
        my $minimumValidationFlexibility = 120;
        if $duration > 3600
        {
            $minimumValidationFlexibility = 240;
        }
        if ( $duration+$minimumValidationFlexibility > $maxDuration)
        {
            $maxDuration = $duration+$minimumValidationFlexibility;
        }
        if ( $duration-$minimumValidationFlexibility < $minDuration)
        {
            $minDuration = $duration-$minimumValidationFlexibility;
        }
        if %fileMeta<video> < $minDuration && %fileMeta<video> > $maxDuration
        {
            if %fileMeta<general>.defined && %fileMeta<general> <  $minDuration && %fileMeta<general> > $maxDuration
            {
                self.printd: "$file: <video> element failed track length validation";
                self.printd: "The video track was reported to be "~%fileMeta<video>~" seconds. This was compared to max[$maxDuration] and min[$minDuration], reported duration from scan was $duration";
                self.printd: "However, when comparing <general> instead of <video> it succeeded, since <general> is "~%fileMeta<general>~" seconds.";
                self.printd: "This is odd, but will be treated as a length validation success.";
            }
            else
            {
                self.printv: "$file: failed due to video track length validation";
                self.printd: "The video track was reported to be "~%fileMeta<video>~" seconds. This was compared to max[$maxDuration] and min[$minDuration], reported duration from scan was $duration";
                self.printd: '%fileMeta: '~%fileMeta.raku;
                if $discTitle.defined
                {
                    self.printd: 'discTitle: '~$discTitle.dump;
                }
                return False;
            }
        }
        self.printd: $file~': succeeded track length validation';
        if %fileMeta<FrameRate_Minimum>:exists
        {
            if %fileMeta<FrameRate_Minimum> < 5
            {
                self.printv: "$file: warning: minumum framerate is "~%fileMeta<FrameRate_Minimum>~". This might indicate a broken rip";
                self.printd: "The video framerate was reported to"~%fileMeta<FrameRate_Minimum>~". This is below the threshold of 5";
            }
        }
        return True;
    }

    # This is a simple algorithm meant to verify that a file is at least of a minimum
    # file size that we expect a DVD rip to be.
    #
    # 42 minutes of video is expected to be at least 190MiB
    # 42*60 is 2520 seconds
    # 190/2520 = 0.0753 =~ 0.075MiB/second
    # Thus a file is expected to be at least length*0.075 MiB in size.
    my Rat constant $sizePerSecond = 0.075;

    my Int $minSizeBytes = (($duration*$sizePerSecond)*1048576).Int;

    if $file.IO.s < $minSizeBytes
    {
        self.printd: "$file: is smaller than the minimum file size expected for this file ($minSizeBytes bytes)";
        return False;
    }
    else
    {
        self.printd: $file~': succeeded size validation';
    }

    # Drop "bytesPerSecond" checks if we have no $largestRipped
    if $largestRipped == 0.0
    {
        return True;
    }

    my Int $validationTime = ($largestRipped.Int*0.70).Int;
    my Int $bytesPerSecond = ($file.IO.s / $duration).Int;

    if $bytesPerSecond >= $largestRipped
    {
        self.printd: $file~': succeeded bytesPerSecond test';
        return True;
    }
    else
    {
        self.printv: "$file: failed due to bytesPerSecond comparison";
        self.printd: "bytesPerSecond[$bytesPerSecond] < largestRipped[$largestRipped]";
        return False;
    }

}

#| Run a foreground process
#| pseudoPassthroughStdout is ignored if verbosity > 199
method runForegroundProcess (@command, Int :$timeoutGuard is copy = 0, Bool :$pseudoPassthroughStdout, Bool :$autoTimeoutGuard)
{
    my $state;
    my $stdout = 'passthrough';
    if $pseudoPassthroughStdout
    {
        $stdout = 'pseudo-passthrough';
    }
    if $autoTimeoutGuard && $.timeout-guard
    {
        $timeoutGuard = self.getTimeoutGuard();
    }
    if $.verbosity >= 199
    {
        $state = self.runSubProcess(@command, :$timeoutGuard);
    }
    else
    {
        $state = self.runSubProcess(@command, :stderr<discard>, :$timeoutGuard, :$stdout);
    }
    return $state.success;
}

#| Resolve the "main title" or episodes for a TV series
method resolveTitles (%scanData, Bool :$silent = False)
{
    my @titles;
    # In debug mode, output some useful info
    if $.verbosity >= 99 && %scanData<titleOrder>.defined
    {
        for @(%scanData<titleOrder>) -> $title
        {
            self.printd('Identified title '~$title~' [length: '~%scanData<titles>.{$title}.totalInSeconds~']');
        }
    }
    if self.isMode('tv')
    {
        @titles = self.resolveTitlesSeries(%scanData, :$silent);
    }
    elsif self.isMode('movie')
    {
        @titles = self.resolveTitleMovie(%scanData, :$silent);
    }
    else
    {
        self.errOut: "resolveTitles(): unable to determine mode with isMode()", :internalError;
    }
    if @titles > 15
    {
        self.errOut: "Disc looks like it has DRM-by-obscurity (found "~@titles.elems~" titles), you will need to\nspecify titles manually. Exiting.", canRecover => True;
    }
    return @titles;
}

#| Resolve the "main title" for a movie
method resolveTitleMovie (%scanData, :$silent)
{
    $.guessedMainTitle = False;
    $.likelyWrongGuess = False;
    my @titles;
    for @(%scanData<titleOrder>) -> $title
    {
        if (%scanData<titles>.{$title}.defined && %scanData<titles>.{$title}.mainTitle)
        {
            @titles.push($title);
        }
    }
    if @titles.elems == 0
    {
        my @possibleTitles;
        my $longest;
        my $nextLongest;

        for @(%scanData<titleOrder>) -> $title
        {
            if $longest.defined
            {
                if %scanData<titles>.{$longest}.idString && %scanData<titles>.{$title}.idString && %scanData<titles>.{$title}.idString eq %scanData<titles>.{$longest}.idString
                {
                    self.printd: "Ignoring title $title for the purposes of main title detection, it has an idString identical to $longest";
                }
                elsif %scanData<titles>.{$title}.totalInSeconds > %scanData<titles>.{$longest}.totalInSeconds
                {
                    $nextLongest = $longest;
                    $longest = $title;
                }
                elsif $nextLongest.defined
                {
                    if %scanData<titles>.{$title}.totalInSeconds >= %scanData<titles>.{$nextLongest}.totalInSeconds
                    {
                        $nextLongest = $title;
                    }
                }
                else
                {
                    $nextLongest = $title;
                }
            }
            else
            {
                $longest = $title;
            }
        }
        if $longest.defined && $nextLongest.defined
        {
            my $longestLen = %scanData<titles>.{$longest}.totalInSeconds;
            my $nextLongestLen = %scanData<titles>.{$nextLongest}.totalInSeconds;
            my $longestLen70 = $longestLen * 0.70;
            if $longestLen70 >= $nextLongestLen
            {
                self.printd: "Fallback detection of title (using the 70% method) found title '$longest'";
                @titles.push($longest);
            }
            elsif (%scanData<titles>.{$longest}.totalInSeconds - 1800) > %scanData<titles>.{$nextLongest}.totalInSeconds
            {
                self.printd: "Fallback detection of title (using the 30-minute-method) found title '$longest'";
                @titles.push($longest);
            }
            else
            {
                my $pickedBasedOnSubAndAudioTracks = False;
                if (%scanData<titles>.{$longest}.totalInSeconds == %scanData<titles>.{$nextLongest}.totalInSeconds || %scanData<titles>.{$longest}.totalInSeconds-60 < %scanData<titles>.{$nextLongest}.totalInSeconds)
                {
                    # They're the same, so pick the one with the most audio and subtitle tracks
                    my $firstTotal = %scanData<titles>.{$longest}.subtitles.elems + %scanData<titles>.{$longest}.audio.elems;
                    my $secondTotal = %scanData<titles>.{$nextLongest}.subtitles.elems + %scanData<titles>.{$nextLongest}.audio.elems;

                    if ($firstTotal != $secondTotal)
                    {
                        self.printd: "Fallback detection using the number of audio and subtitle tracks. Picking the one of our two candidates with the most tracks.";
                        $pickedBasedOnSubAndAudioTracks = True;
                        if ($firstTotal > $secondTotal)
                        {
                            self.printd: "Picking track $longest";
                            @titles.push($longest);
                        }
                        else
                        {
                            self.printd: "Picking track $nextLongest";
                            @titles.push($nextLongest);
                        }
                    }
                }
                else
                {
                    self.printd: "Can't use audio/subtitle-based fallback, the two best tracks are not within 60 seconds of each other in length: "~%scanData<titles>.{$longest}.totalInSeconds~" and "~%scanData<titles>.{$nextLongest}.totalInSeconds;
                }
                if !$pickedBasedOnSubAndAudioTracks
                {
                    # TODO: Loop over all titles here, if they're all
                    # of the same length, then stupid DRM is in effect
                    # and we can inform the user about it.
                    my %metadata = self.getDiscMetadata();
                    my $discName = "this disc";
                    if %metadata<LABEL>.defined && !self.isGenericDiscName(%metadata<LABEL>)
                    {
                        $discName = '"'~%metadata<LABEL>~'"';
                    }

                    if $.allowGuessing
                    {
                        my $backendGuess = self.getGuessedMainTitle(%scanData);
                        if $backendGuess.defined
                        {
                            $.likelyReliableGuess = True;
                            self.printd: "Using backend guessed title "~$backendGuess;
                            @titles.push($backendGuess);
                        }
                        else
                        {
                            if %scanData<titleOrder>.elems > 40
                            {
                                my $longestLen70 = $longestLen * 0.70;
                                my $atLeast70percent = 0;
                                for @(%scanData<titleOrder>) -> $title
                                {
                                    if %scanData<titles>.{$title}.totalInSeconds >= $longestLen70
                                    {
                                        $atLeast70percent++;
                                    }
                                }
                                if $atLeast70percent > 10
                                {
                                    say "Warning: high number of titles ("~%scanData<titleOrder>.elems~"), high chance of the guessed title ($longest) being wrong.";
                                    $.likelyWrongGuess = True;
                                }
                            }
                            if $longestLen == $nextLongestLen
                            {
                                self.printd: "Guessing that the main title is $longest (even though it is the same length as the next longest, $nextLongestLen)";
                            }
                            else
                            {
                                self.printd: "Guessing that the main title is $longest";
                            }
                            if $longestLen < 1200
                            {
                                if $silent
                                {
                                    return @titles;
                                }
                                self.errOut: "Found no 'main title' on $discName, and our guessed 'main title' is only $longestLen seconds. Broken disc?", canRecover => True;
                            }
                            @titles.push($longest);
                        }
                        $.guessedMainTitle = True;
                        return @titles;
                    }

                    self.printd: "The detected longest title ($longest, $longestLen seconds) was not 30% longer than the next longest track ($nextLongest, $nextLongestLen seconds). Fallback detection failed.";
                    if $silent
                    {
                        return @titles;
                    }
                    self.errOut: "Found no 'main title' on $discName, and fallback tests failed.\nTry specifying the title number manually.", canRecover => True;
                }
            }
        }
        elsif $longest.defined
        {
            @titles.push($longest);
        }
        else
        {
            self.errOut: "No titles were found on this disc. This is probably a lazybrake bug.", :canRecover, :internalError;
        }
    }
    return @titles;
}

#| Resolve the episodes for a TV series
method resolveTitlesSeries (%scanData, :$silent)
{
    my @titles;
    if $.max-duration || $.min-duration
    {
        for @(%scanData<titleOrder>) -> $title
        {
            my $discTitle = %scanData<titles>.{$title};
            my $totalInSeconds = $discTitle.totalInSeconds;
            if $totalInSeconds <= $.max-duration && $totalInSeconds >= $.min-duration
            {
                @titles.push($title);
            }
            else
            {
                if $.chapterHack && $discTitle.chapterHackPossible
                {
                    @titles.push($title);
                    self.printd: "Would have skipped title "~$title~" but chapter hack is enabled, so keeping it";
                }
                else
                {
                    my $too = 'long';
                    if $totalInSeconds < $.min-duration
                    {
                        $too = 'short';
                    }
                    my $formattedLength = sprintf "%02d:%02d:%02d", $discTitle.Hours, $discTitle.Minutes, $discTitle.Seconds;
                    my $message = "Skipping title $title because it is too $too ("~$discTitle.totalInSeconds~" seconds ["~$formattedLength~"])";
                    if $silent
                    {
                        self.printd: $message;
                    }
                    else
                    {
                        self.printv: $message;
                    }
                }
            }
        }
    }
    else
    {
        @titles = @(%scanData<titleOrder>);
    }
    return @titles;
}

#| Retrieve a timeout guard value
method getTimeoutGuard (Bool :$Scanning = False) returns Int
{
    if (!$.timeout-guard)
    {
        return 0;
    }
    # For file sources, don't apply a timeout
    if $.device.IO.e && $.device.IO.f
    {
        return 0;
    }
    if $Scanning
    {
        return 600; # 10 minutes
    }
    if ($.movie)
    {
        return 9000; # 2.5 hours
    }
    if ($.tv)
    {
        return 5400; # 1.5 hours
    }
    self.errOut: "getTimeoutGuard() is confused - we're not .movie nor .tv", :internalError;
}

#| Resets base values to default
method resetBaseBackendAttributes ()
{
    $.bdEncoder = False;
    $.guessedMainTitle = False;
    $.likelyWrongGuess = False;
    $.likelyReliableGuess = False;
}

#| Return the backends guessed title, or Nil. Implemented by some backends.
method getGuessedMainTitle (%scanData)
{
    return Nil;
}

# ---
# The follwing methods are required in order to implement the backend interface
# ---

method usable () returns Bool
{
    ...
}

method scan ($gentle = False) returns Hash
{
    ...
}

method ripSingleTitle(Int :$title!, :$filename!, :$discTitle!, :$chapter = Nil, Int :$jobNumber = 0, Int :$elementsToRip = 1, Bool :$no-dvdnav is copy = False) returns Hash
{
    ...
}

method extension ()
{
    ...
}
