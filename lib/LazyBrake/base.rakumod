#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#|{ Base class for all of our other classes
  Provides all of our helpers (like printv/printd) as well as class variables
  and other methods (ie. validateFile()) that all subclasses need}
unit class LazyBrake::base;

use LazyBrake::Logger;
use LazyBrake::RecoverableError;
use LazyBrake::SubprocessReturnV;
use NativeCall;
use JSON::Fast;

# NativeCall kill
sub kill(int32, int32) is native {*};

has Int  $.verbosity is rw = 0;
has Int  $.min-duration = 900;
has Int  $.max-duration;
has Bool $.try-dvdnav = True;
has Str  $.device;
has Str  $.name;
has Bool $.auto-debug = False;
has Bool $.no-dvdnav   = False;
has Bool $.dry-run = False;
has Bool $.two-pass    = False;
has Bool $.permitEject = True;
has Bool $.auto-deinterlace = False;
has Bool $.looping is rw = False;
has Bool $.validateFiles = True;
has Bool $.movie;
has Bool $.tv;
has Bool $.timeout-guard = False;
has Bool $.inclusive-lang = False;
has Bool $.chapterHack = False;
has Int  $.chapterHackExpectedTitles = 0;
has Bool $.autoChapterHack = False;
has Bool $.optimize-film-grain is rw = False;
has Bool $.allowGuessing = False;
has Bool $.auto-optimize-film-grain = False;
has Str $.exec-on-done;
has Str $.subtitle;
has Str $.audiolang;
has Str $.hdb-preset is required;
has Str $.hdb-preset-bd;
has Str $.nlmeans is rw;
has Bool $.containErrors is rw = False;
has Supply $.appEvents is rw;
has Supplier $.appEventsSupplier is rw;
has @!signalHandlers;
has LazyBrake::Logger $.logger;
#| Must be overridden by subclasses
has @.dependencies;

method TWEAK ()
{
    # Bump verbosity to 99 if we receive a SIGUSR1
    @!signalHandlers.push: signal(SIGUSR1).tap({
        if $.verbosity < 99
        {
            $.verbosity = 99;
            # If we're the main class, then we also output a message
            if self.^name eq 'LazyBrake'
            {
                self.printd: "Increased verbosity to 99 because of SIGUSR1";
            }
        }
    });
}

method DESTROY ()
{
    for @!signalHandlers -> $handler
    {
        $handler.close;
    }
}

#| Output debugging text
method printd(Str $message)
{
    self.printv('Debug: '~$message,99);
}

method autoOutputLog (Str :$reason)
{
    if $.verbosity < 99 && $.auto-debug
    {
        if $reason.defined
        {
            self.printv: "Auto-outputting debug log: "~$reason;
        }
        $.appEventsSupplier.emit('SPURT');
    }
}

#| Output verbose text
method printv(Str $message, Int $level = 1)
{
    my Str $prefix = '';
    if $level >= 99
    {
        $prefix = '['~self.^name~'] ';
    }
    $.logger.add($prefix~$message);
    if $.verbosity >= $level
    {
        print $message~"\n";
    }
}

#| Output text in non-verbose mode. Note: does not append \n
method printnv(Str $message)
{
    if $.verbosity < 1
    {
        print $message;
    }
}

#| Error out. If canRecover is true then a LazyBrake::RecoverableError exception will
#| be thrown, indicating to our caller that the error is specific to this
#| disc, allowing us to recover from the error by ejecting the disc and
#| getting a new disc from the user.
method errOut(Str $reason is copy, Int $retVal = 1, Bool :$canRecover = False, Bool :$internalError = False, Bool :$couldBeBug = False)
{
    # Force dump the log if it's an internal error and we're either not able to
    # recover, or not in $.containErrors mode.
    if ($internalError || $couldBeBug) && (!$canRecover || !$.containErrors)
    {
        $.logger.outputLog();
    }
    # If we can recover and are either looping or containing errors, then throw
    # a LazyBrake::RecoverableError
    if $canRecover && ( $.looping || $.containErrors )
    {
        (my $printdReason = $reason) ~~ s:g/\n/\\n/;
        self.printd("Throwing LazyBrake::RecoverableError: "~$printdReason);
        LazyBrake::RecoverableError.new(
            :message($reason)
        ).throw;
    }
    else
    {
        $*ERR.print($reason~"\n");
        self.runExecOnDone($reason);
        exit $retVal;
    }
}

method runExecOnDone(Str $message)
{
    if $.exec-on-done.defined
    {
        if self.inPath($.exec-on-done) || ($.exec-on-done.IO.e && $.exec-on-done.IO.x)
        {
            self.runSubProcess([$.exec-on-done,$message], :timeoutGuard<30>);
        }
        else
        {
            say "Warning: unable to run --exec-on-done command "~$.exec-on-done~": either isn't in path, or not executeable";
        }
    }
}

#| Checks if a command is in our PATH
method inPath ($command)
{
    for %*ENV<PATH>.split(':') -> $path
    {
        if ($path~'/'~$command).IO.x
        {
            return True;
        }
    }
    self.printd: $command~': not found in PATH';
    return False;
}

#| A generic dependency checker that just loops through @.dependencies
method usable () returns Bool
{
    for @.dependencies -> $command
    {
        if $command ~~ List
        {
            my $found = False;
            for $command.List -> $subCommand
            {
                if self.inPath($subCommand)
                {
                    $found = True;
                }
            }
            if ! $found
            {
                return False;
            }
        }
        elsif !self.inPath($command)
        {
            return False;
        }
    }
    return True;
}

#| Check if we are in tv or movie mode
method isMode(Str $checkType) returns Bool
{
    if $.movie && $.tv
    {
        self.errOut('Both movie and tv is True. LazyBrake-class construction error', :internalError);
    }
    if $checkType eq 'tv' && $.tv
    {
        return True;
    }
    elsif $checkType eq 'movie' && $.movie
    {
        return True;
    }
    return False;
}

#| Parse a timestamp (HH:MM:SS or HH:MM:SS.xx)
method parseTimestamp(Str $timestamp is copy) returns Hash
{
    try
    {
        CATCH
        {
            default
            {
                self.printd('ERROR parsing '~$timestamp);
                .rethrow;
            }
        }
        $timestamp ~~ s/\.\d+$//;
        my $time = $timestamp.split(':');
        my Int $len = ( +$time[2] ) + (+$time[1] *60) + ((+$time[0] *60)*60);
        my %parsed = (
            hours          => $time[0].Int,
            minutes        => $time[1].Int,
            seconds        => $time[2].Int,
            totalInSeconds => $len.Int,
        );
        return %parsed;
    }
}

#| Instantiate a backend
method instantiateBackend ($backendClass, %additionalAttributes?)
{
    return $backendClass.new(
        :$.try-dvdnav,
        :$.min-duration,
        :$.max-duration,
        :$.device,
        :$.verbosity,
        :$.no-dvdnav,
        :$.timeout-guard,
        :$.inclusive-lang,
        :$.two-pass,
        :$.auto-deinterlace,
        :$.subtitle,
        :$.audiolang,
        :$.tv,
        :$.movie,
        :$.looping,
        :$.appEvents,
        :$.appEventsSupplier,
        :$.chapterHack,
        :$.autoChapterHack,
        :$.chapterHackExpectedTitles,
        :$.name,
        :$.hdb-preset,
        :$.hdb-preset-bd,
        :$.logger,
        :$.optimize-film-grain,
        :$.auto-optimize-film-grain,
        :$.permitEject
        :$.dry-run
        :$.exec-on-done,
        :$.allowGuessing,
        :$.nlmeans,
        parent => self,
        |%additionalAttributes
    );
}

#| Save a lazybrake backup file.
#| If one already exists, it will save the largest file of the two (either the
#| existing backup or the existing rip).
method saveBackupFile (Str $baseName)
{
    my $backupFilename = self.getBackupFilename($baseName);
    if (!$baseName.IO.e)
    {
        if $backupFilename.IO.e
        {
            return $backupFilename;
        }
        else
        {
            return Nil;
        }
    }
    if ($backupFilename.IO.e)
    {
        if $backupFilename.IO.s < $baseName.IO.s
        {
            $backupFilename.IO.unlink;
            $baseName.IO.rename($backupFilename);
        }
        else
        {
            $baseName.IO.unlink;
        }
    }
    else
    {
        $baseName.IO.rename($backupFilename);
    }
    return $backupFilename;
}

#| Get the backup variant filename for a rip
method getBackupFilename (Str $baseName) returns Str
{
    return $baseName~'-lazybrake.validation.backup~';
}

#| Function that forces a Proc::Async to exit
method !killProcAsyncProcess (Proc::Async $proc, Promise $promise, :$name is required, :$type where {'wait', 'timeout'} = 'wait')
{
    my Bool $message = False;
    my $PID = $proc.ready.result;
    if !$promise.Bool
    {
        if $type eq 'timeout'
        {
            self.printnv: "(timed out, killing subprocess ($name) ";
        }
        else
        {
            self.printnv: "(waiting for subprocess ($name) ";
        }
        $message = True;
    }
    for 1..10
    {
        if $promise.Bool
        {
            last;
        }
        self.printd: "Sending SIGINT to subprocess ["~$PID~"].";
        kill($PID,2);
        sleep(1);
        self.printnv: ".";
    }
    for 1..10
    {
        if $promise.Bool
        {
            last;
        }
        self.printd: "Sending SIGTERM to subprocess ["~$PID~"].";
        kill($PID,15);
        sleep(1);
        self.printnv: ".";
    }
    for 1..10
    {
        if $promise.Bool
        {
            last;
        }
        self.printd: "Sending SIGKILL to subprocess ["~$PID~"].";
        kill($PID,9);
        sleep(1);
        self.printnv: ".";
    }
    for 1..5
    {
        if $promise.Bool
        {
            last;
        }
        sleep(1);
        self.printnv: ".";
    }
    if $message
    {
        if $promise.Bool
        {
            self.printnv: "done)\n";
        }
        else
        {
            self.printnv: "FAILED - process still alive)\n";
        }
    }
}

#|{ Function that runs a process, optionally capturing the input from it.
    Most of the modes are self-explanatory, the exception being pseudo-passthrough. This
    will read stdout from the process, line by line, and then output it, but replacing
    the previous line each time.
#|}
method runSubProcess (@cmd,:$stdout where {'pseudo-passthrough' | 'passthrough' | 'capture' | 'discard'} = 'passthrough', :$stderr where {'passthrough' | 'capture' | 'discard'} = 'passthrough', Int :$timeoutGuard = 0) returns LazyBrake::SubprocessReturnV
{
    my $out;
    my Bool $success = True;
    my Bool $sigQUIT = False;
    state $devNull;
    if !$devNull.defined || !$devNull.opened
    {
        $devNull = $*SPEC.devnull.IO.open;
    }
    # Mask any localization
    my $LC_ALL = %*ENV<LC_ALL>;
    %*ENV<LC_ALL> = 'C';
    self.printd: "Running: "~@cmd.join(' ');
    self.printd: "runSubProcess settings: stdout=$stdout, stderr=$stderr, timeoutGuard=$timeoutGuard";
    my $proc = Proc::Async.new(@cmd, :enc<utf8-c8>);
    my $name = @cmd[0];
    my @output;
    if $stdout eq 'capture'
    {
        $proc.stdout.tap(-> $line { @output.push($line) });
    }
    if $stderr eq 'capture'
    {
        $proc.stderr.tap(-> $line { @output.push($line) });
    }
    if $stdout eq 'pseudo-passthrough'
    {
        my $previous = 0;
        $proc.stdout.tap(-> $line {
            for $line.split(/\n/) -> $part is copy
            {
                next if $part.chars == 0;
                for 1..$previous
                {
                    print "\b \b";
                }
                print $part;
                $previous = $part.chars;
            }
        });
    }
    if $stdout eq 'discard'
    {
        $proc.bind-stdout($devNull);
    }
    if $stderr eq 'discard'
    {
        $proc.bind-stderr($devNull);
    }
    my $promise = $proc.start;
    # LazyBrake emits a QUIT event if it receives a SIGINT. We listen for that
    # and kill our subprocess if it's still running when we receive the QUIT
    # message.
    my $eventHandler = $.appEvents.tap(-> $ev {
        if ($ev eq 'QUIT')
        {
            $sigQUIT = True;
            self!killProcAsyncProcess($proc, $promise, :$name);
        }
    });
    try
    {
        CATCH
        {
            default
            {
                self.printd( "runSubProcess() caught error "~$_);
                $success = False;
                return LazyBrake::SubprocessReturnV.new(
                    :success(False),
                    :exitcode(999),
                    :content('')
                );
            }
        }
        if $timeoutGuard > 0
        {
            sink await Promise.anyof(
                Promise.in($timeoutGuard),
                $promise
            );
            if !$promise.Bool
            {
                self.printd: "Command timed out, killing it.";
                self!killProcAsyncProcess($proc,$promise, :$name, :type<timeout>);
                $eventHandler.close;
                return LazyBrake::SubprocessReturnV.new(
                    :success(False),
                    :exitcode(999),
                    :content('')
                );
            }
        }
        else
        {
            sink await $promise;
        }
    }
    # Sleep forever to avoid the current thread doing stuff while another is
    # exiting lazybrake
    if $sigQUIT
    {
        self.printd: "Main thread going to sleep";
        sleep Inf;
    }
    $eventHandler.close;
    # Reset LC_ALL again
    %*ENV<LC_ALL> = $LC_ALL;
    my Int $exitcode = $promise.result.exitcode;
    if ($exitcode != 0)
    {
		$success = False;
    }
    if $stdout eq 'pseudo-passthrough'
    {
        print "\n";
    }
    self.printd( 'Subprocess exited with '~$promise.result.exitcode ) ;
    return LazyBrake::SubprocessReturnV.new(
        :$success,
        :$exitcode,
        content => @output.join('')
    );
}

#| Generic function to read input from a subprocess
method getInputFrom (@cmd,$capture = 'out', Int :$timeoutGuard = 600) returns Str
{
    my $stdout = 'discard';
    my $stderr = 'discard';
    if $capture eq 'out' || $capture eq 'both'
    {
        $stdout = 'capture';
    }
    if $capture eq 'err' || $capture eq 'both'
    {
        $stderr = 'capture';
    }
    if $capture ne 'both' && $capture ne 'err' && $capture ne 'out'
    {
        self.errOut: 'Unknown capture: '~$capture, :internalError;
    }
    my $status = self.runSubProcess(@cmd,
        :$stdout,
        :$stderr,
        :$timeoutGuard
    );
    return $status.content;
}

grammar basicKeyValue
{
    rule TOP
    {
        <key>\=<value>\n?
    }
    rule key
    {
        <-[=]>+
    }
    rule value
    {
        .+
    }
}

#| Retrieve metadata about the inserted disc using udevadm
method !getDiscMetadataFromUdev(Bool :$bypass-cache = False) returns Hash
{
    state %cache = (
        changed => 0,
        data => Nil
    );
    state $useFallbackMethod = False;
    if $useFallbackMethod
    {
        return self!getDiscMetadataFromBlk(:$bypass-cache);
    }
    # If it has changed the last three seconds, use the cached one
    if %cache<changed> > (time - 3) && !$bypass-cache
    {
        self.printd: 'getDiscMetadataFromUdev(): using cached disc metadata';
        return %cache<data>;
    }
    my %meta = (
        BD => False,
        DVD => False,
        FILE => False,
        MEDIA => False,
        LABEL => Nil,
        UUID => Nil,
    );
    self.printd: 'getDiscMetadataFromUdev(): Asking udevadm for disc metadata';
    my $data = self.getInputFrom(['udevadm','info','-q','env','-n',$.device],'out', :timeoutGuard(120));
    my $validUdevadm = False;
    for $data.lines -> $line
    {
        my $parsed = basicKeyValue.parse($line);
        given $parsed<key>
        {
            when 'ID_CDROM_MEDIA'
            {
                %meta<MEDIA> = $parsed<value> eq '1';
            }
            when 'ID_CDROM_MEDIA_DVD'
            {
                %meta<DVD> = $parsed<value> eq '1';
            }
            when 'ID_CDROM_MEDIA_BD'
            {
                %meta<BD> = $parsed<value> eq '1';
            }
            when 'ID_FS_LABEL'
            {
                %meta<LABEL> = $parsed<value>.Str;
            }
            when 'ID_FS_UUID'
            {
                %meta<UUID> = $parsed<value>.Str;
            }
            when 'DEVLINKS' | 'ID_DROM_DVD'
            {
                $validUdevadm = True;
            }
        }
    }
    if ! %meta<LABEL>.defined
    {
        %meta<LABEL> = 'UnknownDisc-'~time.fmt;
    }
    if ! %meta<UUID>.defined
    {
        %meta<UUID> = time.fmt;
    }
    if !$validUdevadm && self.inPath('lsblk') && self.inPath('blkid')
    {
        $useFallbackMethod = True;
        if self.^name eq 'LazyBrake'
        {
            self.printd("Udev appears to not be working properly (container environment?). Switching to lsblk.");
        }
        return self!getDiscMetadataFromBlk(:$bypass-cache);
    }
    # If we have a BD or DVD, store this in the cache
    if %meta<DVD> || %meta<BD>
    {
        %cache<changed> = time;
        %cache<data> = %meta;
        # Log the data we got
        self.printd: 'Got metadata from udevadm: '~%meta.gist;
    }
    return %meta;
}

#|{ Retrieve metadata about the inserted disc using lsblk and blkid
#|}
method !getDiscMetadataFromBlk(Bool :$bypass-cache = False) returns Hash
{
    state %cache = (
        changed => 0,
        data => Nil
    );
    # If it has changed the last three seconds, use the cached one
    if %cache<changed> > (time - 3) && !$bypass-cache
    {
        self.printd: 'getDiscMetadataFromBlkid(): using cached disc metadata';
        return %cache<data>;
    }
    my %meta = (
        BD => False,
        DVD => False,
        FILE => False,
        MEDIA => False,
        LABEL => Nil,
        UUID => Nil,
    );
    my %deviceData = self!getRawLsblkData();
    if ! %deviceData.defined
    {
        self.errOut: "Data from lsblk is not in the proper format";
    }
    my $devSizeGB = -1;
    if %deviceData<size>.defined
    {
        $devSizeGB = ((%deviceData<size>/1024)/1024)/1024;
    }
    # DVDs have a max size of approx 8.5GB. Therefore we assume BD over 9 GB
    if $devSizeGB > 9
    {
        %meta<BD> = True;
    }
    # UDF versions 2.50 and 2.60 are used on BDs (and HD-DVDs, but we don't
    # handle that at all, so we use it to assume BD)
    elsif %deviceData<fsver>.defined && ( %deviceData<fsver> eq '2.50' || %deviceData<fsver> eq '2.60' )
    {
        %meta<BD> = True;
    }
    # Otherwise, as long as fstype is udf, we assume DVD
    elsif %deviceData<fstype>.defined && %deviceData<fstype> eq 'udf'
    {
        %meta<DVD> = True;
    }
    # Fall back to blkid data if lsblk data is invalid
    #
    # Missing lsblk data could be that there's no disc, but it could also be
    # that lsblk is only partially usable, as it is in some containers.
    #
    # In these cases, we use lsblk data to detect disc type, and blkid data
    # for everything else
    if !%deviceData<fsver>.defined || !%deviceData<fstype>.defined
    {
        my %blkidData = self!getRawBlkidData();
        if %blkidData<VALID>
        {
            %meta<LABEL> = %blkidData<LABEL>;
            %meta<UUID> = %blkidData<UUID>;
            # If we've not already detected a BD due to disc size, assume DVD
            if !%meta<BD> && !%meta<DVD>
            {
                self.printd: "Assuming DVD because of low-quality lsblk data and disc size under 9GB";
                %meta<DVD> = True;
            }
        }
        else
        {
            %meta<BD> = False;
            %meta<DVD> = False;
        }
    }
    # lsblk data valid, use it
    else
    {
        %meta<LABEL> = %deviceData<label>;
        %meta<UUID>  = %deviceData<uuid>;
    }
    # Generate a LABEL if missing
    if ! %meta<LABEL>.defined
    {
        %meta<LABEL> = 'UnknownDisc-'~time.fmt;
    }
    # Generate a UUID if missing
    if ! %meta<UUID>.defined
    {
        %meta<UUID> = time.fmt;
    }
    # If we have valid data about a disc, cache it
    if %meta<DVD> || %meta<BD>
    {
        %cache<changed> = time;
        %cache<data> = %meta;
        # Log the data we got
        self.printd: 'Got metadata from blk: '~%meta.gist;
    }
    # Return the data
    return %meta;
}

method !getRawLsblkData () returns Hash
{
    my %lsblkData;
    my $data = self.getInputFrom(['lsblk','--output','UUID,NAME,SIZE,LABEL,FSVER,FSTYPE','--bytes','--json',$.device],'out', :timeoutGuard(120));
    try
    {
        CATCH
        {
            default
            {
                self.printd: "Failed to parse JSON from lsblk: "~.message;
                return {};
            }
        }
        %lsblkData = from-json($data).hash;
    }
    self.printd: 'getRawLsblkData(): got '~%lsblkData<blockdevices>.[0].raku;
    if !%lsblkData<blockdevices>.[0].defined
    {
        return {};
    }
    return %lsblkData<blockdevices>.[0];
}

method !getRawBlkidData () returns Hash
{
    my %meta = (
        VALID => False,
        LABEL => Nil,
        UUID => Nil,
    );
    my $data = self.getInputFrom(['blkid','-o','export',$.device],'out', :timeoutGuard(120));
    my $validBlkid = False;
    for $data.lines -> $line
    {
        my $parsed = basicKeyValue.parse($line);
        given $parsed<key>
        {
            when 'UUID'
            {
                %meta<UUID> = $parsed<value>.Str;
                %meta<VALID> = True;
            }
            when 'LABEL'
            {
                %meta<LABEL> = $parsed<value>.Str;
                %meta<VALID> = True;
            }
            when 'TYPE'
            {
                %meta<VALID> = True;
            }
        }
    }
    self.printd: 'getRawBlkidData(): got '~%meta.raku;
    return %meta;
}

#| Retrieve metadata about the inserted disc
method getDiscMetadata(Bool :$bypass-cache = False) returns Hash
{
    if !$.device.defined
    {
        self.printd: "getDiscMetadata(): \$.device is Nil for some reason, returning stubbed data";
        my %meta = (
            VALID => False,
            LABEL => Nil,
            UUID => Nil,
        );
        return %meta;
    }
    # if it's a regular file, then it's not a device, use file info instead
    if $.device.IO.e && $.device.IO.f
    {
        self.printd: "getDiscMetadata(): "~$.device~": is a file, not a device, faking information.";
        my $file = $.device.IO.basename;
        my $dir = $.device.IO.dirname;
        $dir ~~ s:g/\///;

        my %basicMeta = (
          UUID => $dir,
          LABEL => $file,
          BD => False,
          DVD => False,
          FILE => True
        );
        return %basicMeta;
    }
    return self!getDiscMetadataFromUdev(:$bypass-cache);
}

method bytesToHumanReadable(Int $bytes)
{
    my $oneGB = 1_073_741_824;
    my $oneMB = 1_048_576;
    if $bytes >= $oneGB
    {
        my $size = $bytes/$oneGB;
        return $size.round(0.01)~'GiB';
    }
    elsif $bytes >= $oneMB
    {
        my $size = $bytes/$oneMB;
        return $size.round(0.01)~'MiB';
    }
    else
    {
        return $bytes~'B';
    }
}

#| Check if the $name is a generic name or not (ie. if it actually says
#| something about the content of the disc). Returns True if it *is* generic.
method isGenericDiscName(Str $name) returns Bool
{
    return $name.starts-with("DVD", :i) || $name.starts-with("UnknownDisc", :i) || $name.starts-with("UNDEFINED", :i) || $name.starts-with("LOGICAL_VOLUME_ID",:i);
}


#| Check if a disc is present in the drive
method isDiscPresent(Bool :$bypass-cache = False) returns Bool
{
    my %info = self.getDiscMetadata(:$bypass-cache);
    return ( %info<MEDIA> || %info<DVD> || %info<BD> || %info<FILE> );
}

#| Eject a disc
method eject (Bool :$wait = False, Bool :$tolerate-failure = False)
{
    if $.permitEject
    {
        if $.dry-run && !$.looping
        {
            say "Would have ejected the disc if not for --dry-run"
        }
        else
        {
            if ! self.isDiscPresent(:bypass-cache)
            {
                return True;
            }
            for 1..10 -> $attempt
            {
                self.printd: "Running: eject "~$.device;
                try
                {
                    CATCH
                    {
                        self.printd: "ejecting failed";
                    }
                    my $proc = run 'eject', $.device;
                    if $proc.exitcode != 0
                    {
                        self.printd: "ejecting failed (returned "~$proc.exitcode~')';
                    }
                    elsif !self.isDiscPresent(:bypass-cache)
                    {
                        return True;
                    }
                    else
                    {
                        self.printd: "disc still present after eject, going to retry",
                    }
                }
                sleep(5);
            }
            if self.isDiscPresent(:bypass-cache)
            {
                if $tolerate-failure
                {
                    return False;
                }
                self.errOut("Failed to eject disc. Unable to continue safely.",3, :couldBeBug);
            }
        }
    }
    return True;
}
