#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::EntryDuration;
use LazyBrake::DVDChapter;
#| Representation of a single chapter of a DVD
class LazyBrake::DiscTitle is LazyBrake::EntryDuration
{
    has Int $.titleNo is required;
    has Bool $.mainTitle is rw = False;
    has %.subtitles;
    has %.audio;
    has Bool $.interlaced = False;
    has Bool $.chapterHackPossible is rw = False;
    has Bool $.chapterHackRecommended is rw = False;
    has LazyBrake::DVDChapter @.chapters is rw;
    has Str $.idString is rw = '';
    has $.owner is rw;

    method printd($message)
    {
        if $.owner.defined
        {
            $.owner.printd($message);
        }
    }

    method dump ()
    {
        return 'titleNo='~$.titleNo~' mainTitle='.$.mainTitle.Str~' idString='~$.idString~' totalInSeconds='~self.totalInSeconds~' Seconds='~$.Seconds~' Minutes='~$.Minutes~' Hours='~$.Hours;
    }
}
