#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

subset Positive of Int where * > -1;

#| Representation of the duration of a title or chapter
class LazyBrake::EntryDuration
{
    has Positive $.Seconds is rw = 0;
    has Positive $.Minutes is rw = 0;
    has Positive $.Hours is rw = 0;
    method totalInSeconds returns Positive
    {
        my Positive $total = 0;
        my $Hours = $.Hours;
        $total = $.Seconds + ($.Minutes*60) + ( ($Hours * 60) * 60);
        return $total;
    }
    method add($Hours is copy,$Minutes is copy,$Seconds is copy)
    {
        $Seconds += $.Seconds;
        $Minutes += $.Minutes;
        $Hours += $.Hours;
        if $Seconds > 59
        {
            my $float = $Seconds/60;
            my $extraMinutes = $float.floor;
            $Seconds = 60*($float-$float.floor).Int;
            $Minutes += $float.floor;
        }
        if $Minutes > 59
        {
            my $float = $Minutes/60;
            my $extraHours = $float.floor;
            $Minutes = 60*($float-$float.floor).Int;
            $Hours += $float.floor;
        }
        $.Seconds = $Seconds;
        $.Minutes = $Minutes;
        $.Hours = $Hours;
    }
    method mergeWith(LazyBrake::EntryDuration $thing)
    {
        $thing.add($.Hours,$.Minutes,$.Seconds);
        return $thing;
    }
}
