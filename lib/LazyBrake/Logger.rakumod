#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#|{ Logger class that handles printv/printd as well as displaying the log
#| when something goes wrong.
#|
#| The logger will keep the N latest log entries, and discard the rest, where
#| N is $.maxLogEntries, defaulting to 250
#| }
unit class LazyBrake::Logger;

has Int $.maxLogEntries = 250;
has @!logEntries;

method add (Str $content)
{
    @!logEntries.push(DateTime.now.truncated-to('second')~': '~$content);
    while @!logEntries.elems > $.maxLogEntries
    {
        @!logEntries.shift;
    }
}

method outputLog ()
{
    .say for @!logEntries;
}
