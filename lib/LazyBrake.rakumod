#!/usr/bin/env raku
# lazybrake - A command-line wrapper for HandBrakeCLI that streamlines and
# automates ripping
#
# Copyright (C) Eskild Hustvedt 2016-2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use LazyBrake::base;
#| Class that handles all of the actual ripping etc.
unit class LazyBrake is LazyBrake::base;

use LazyBrake::RecoverableError;
use LazyBrake::Logger;
use LazyBrake::backend;
use LazyBrake::backend::Auto;
use LazyBrake::backend::HDB;
use LazyBrake::backend::HDB::Smart;
use LazyBrake::backend::makemkv;
use LazyBrake::backend::Hybrid::makemkvHDBEncode;
use LazyBrake::backend::Hybrid::lsdvdscan;
use LazyBrake::RipDiscStatus;
use LazyBrake::RipSingleStatus;
use LazyBrake::base;

has Str  $.subtitle;
has Str  $.audiolang;
has Bool $.scan;
has Str  @.errorDiscs is rw;
has Int  $.discsAttempted is rw = 0;
has Int  $.ripSuccess is rw = 0;
has Str  $.useBackend = 'DEFAULT';
has LazyBrake::Logger $.logger is rw;

method TWEAK ()
{
    $.appEventsSupplier = Supplier.new;
    $.appEvents = $.appEventsSupplier.Supply;
    $.logger = LazyBrake::Logger.new;

    signal(SIGINT).tap({
        print "SIGINT received, exiting...";
        $.appEventsSupplier.emit('QUIT');
        if ($.looping)
        {
            self.summary();
        }
        exit(0);
    });
    signal(SIGUSR2).tap({
        $.logger.outputLog;
    });
    $.appEvents.tap(-> $ev {
        if $.verbosity < 99 && $ev eq 'SPURT'
        {
            $.logger.outputLog;
        }
    });
}

method getBackendByName (Str $name)
{
    if $name eq 'makemkv'
    {
        return LazyBrake::backend::makemkv;
    }
    elsif $name eq 'handbrake' || $name eq 'handbrake-only' || $name eq 'hdb'
    {
        return LazyBrake::backend::HDB;
    }
    elsif $name eq 'hybrid-lsdvd-hdb'
    {
        return LazyBrake::backend::Hybrid::lsdvdscan;
    }
    elsif $name eq 'makemkv-hdb'
    {
        return LazyBrake::backend::Hybrid::makemkvHDBEncode;
    }
    elsif $name eq 'auto'
    {
        return LazyBrake::backend::Auto;
    }
    return Nil;
}

#| Returns our current backend object
method backend ()
{
    state $backend;
    if !$backend
    {
        my %attempted;
        my $brokenBackend;
        my @backends = (
                {
                    name => 'auto',
                    obj => LazyBrake::backend::Auto
                },
            );
        my $requested;
        if $.useBackend && $.useBackend ne 'DEFAULT'
        {
            @backends = ();
            for $.useBackend.split(',') -> $request
            {
                $requested = self.getBackendByName($request);
                if $requested === Nil
                {
                    self.errOut("Unknown --backend: $request");
                }
                self.printd: 'Resolved backend "'~$request~'" to class '~$requested.^name;
                @backends.push({ name => $request, obj => $requested });
            }
        }
        for @backends -> %tryBackend
        {
            my $backendClass = %tryBackend<obj>;
            if !%attempted{$backendClass.^name}
            {
                %attempted{$backendClass.^name} = True;
                $backend = self.instantiateBackend($backendClass);
                if $backend.usable()
                {
                    self.printd: 'Instantiated backend: '~$backend.^name;
                    return $backend;
                }
                else
                {
                    self.printd: 'Backend '~$backend.^name~' reports that it is not usable';
                    $brokenBackend = { objInstance => $backend, name => %tryBackend<name> };
                    $backend = Nil;
                }
            }
        }
        if !$backend
        {
            if $.useBackend
            {
                self.errOut(
                    'Unable to use the backend "'~$brokenBackend<name>~'"'~"\n"~
                    'The following dependencies must be met: '~$brokenBackend<objInstance>.dependencies().join(', ')
                );
            }
            else
            {
                self.errOut('Was unable to find any usable backend. Install HandBrakeCLI and mediainfo to use lazybrake.');
            }
        }
    }
    return $backend;
}

#| Thin wrapper around the backend scan()
method scan ()
{
    return self.backend().scan();
}

#| Thin wrapper around the backend resolveTitles()
method resolveTitles (%scanData, Bool :$silent = False)
{
    return self.backend().resolveTitles(%scanData, :$silent);
}

#| Outputs our summary
method summary ()
{
    say "\r   \r\nSummary:";
    say "Ripped "~$.ripSuccess~" of "~$.discsAttempted~" discs";
    if @.errorDiscs
    {
        say "The following discs failed: "~@.errorDiscs.join(', ');
    }
}

#| Handle ripping of a single disc
method handleDisc (:$name = $.name, :$disc-no, *@titles)
{
    my $started = time;
    print "Scanning disc";
    if ($.looping)
    {
        print " "~$disc-no;
    }
    print "...";
    $.discsAttempted++;
    my %metadata = self.getDiscMetadata();
    print ".";
    my %scanData = self.scan();
    print ".";
    # If we got no titles, sleep one second then retry the scan
    if %scanData<titleOrder>.elems == 0
    {
        sleep 1;
        print ".";
        %scanData = self.scan();
    }
    print "done\n";
    if %scanData<titleOrder>.elems == 0
    {
        self.errOut: "Found no titles on this disc, it does not appear to be a DVD or Blu-ray video disc.", canRecover => True;
    }
    if @titles.elems == 0
    {
        @titles = self.backend().resolveTitles(%scanData);
    }
    if @titles.elems == 0
    {
        self.errOut: "Found no titles on this disc. Try changing --min-duration and --max-duration", canRecover => True;
    }
    my $useName = $name;
    if self.backend().guessedMainTitle
    {
        if self.backend().likelyReliableGuess
        {
            $useName~= '-reliable-guess-';
        }
        else
        {
            $useName~= '-GUESSED-';
            if self.backend().likelyWrongGuess
            {
                $useName ~= '-guess-is-likely-wrong';
            }
        }
    }
    my $ripTitlesResult = self.ripTitles(
        :name($useName),
        :%scanData,
        :@titles,
        volume_id => %metadata<LABEL>,
        volume_uuid => %metadata<UUID>,
    );
    if $ripTitlesResult.success
    {
        $.ripSuccess++;
    }
    if !$ripTitlesResult.hasEjected
    {
        self.eject();
    }

    # Output info on how long the ripping took
    my $spent = time - $started;
    my $hours = (($spent/60)/60).Int;
    my $minutes = (((($spent/60)/60) - $hours)*60).round;
    my Str $message = '';
    my Str $execMessage;
    my $statusMessage = "Finished ripping";
    if !$ripTitlesResult.success
    {
        $statusMessage = "Failed to rip";
    }
    if $disc-no.defined
    {
        $message ~= $statusMessage~" disc $disc-no, it took ";
        if %metadata<LABEL>.defined && !self.isGenericDiscName(%metadata<LABEL>)
        {
            $execMessage = $statusMessage~" \""~%metadata<LABEL>~"\" ($disc-no), it took ";
        }
    }
    else
    {
        $message ~= "Spent ";
        if %metadata<LABEL>.defined && !self.isGenericDiscName(%metadata<LABEL>)
        {
            $execMessage = $statusMessage~" \""~%metadata<LABEL>~"\", it took ";
        }
    }
    if !$execMessage.defined
    {
        $execMessage = $message;
    }
    if $hours > 0
    {
        my $hoursMessage = sprintf "%d %s and ", $hours, ($hours == 1 ?? "hour" !! "hours");
        $message ~= $hoursMessage;
        $execMessage ~= $hoursMessage;
    }
    my $minutesMessage = sprintf "%d %s", $minutes, ($minutes == 1 ?? "minute" !! "minutes");
    $message ~= $minutesMessage;
    $execMessage ~= $minutesMessage;
    if $disc-no.defined
    {
        say $message~".";
    }
    else
    {
        say $message~" on this disc";
    }
    if $.exec-on-done.defined
    {
        if $ripTitlesResult.hasEjected && self.isDiscPresent(:bypass-cache)
        {
            $execMessage ~= " (new disc already inserted)";
        }
        if $ripTitlesResult.fileSize
        {
            my $sizeInGB = (($ripTitlesResult.fileSize/1024)/1024)/1024;
            $execMessage ~= ". Total size "~$sizeInGB~" G.";
        }
        if $ripTitlesResult.errorMessage.defined
        {
            if ! $ripTitlesResult.fileSize
            {
                $execMessage ~= ".";
            }
            $execMessage ~= " "~$ripTitlesResult.errorMessage;
        }
        if self.inPath($.exec-on-done) || ($.exec-on-done.IO.e && $.exec-on-done.IO.x)
        {
            self.runSubProcess([$.exec-on-done,$execMessage], :timeoutGuard<30>);
        }
        else
        {
            say "Warning: unable to run --exec-on-done command "~$.exec-on-done~": either isn't in path, or not executeable";
        }
    }
    if $disc-no.defined
    {
        say "";
    }
}

method getHumanLanguageList(%from)
{
    my %languages;
    for %from.keys -> $entKey
    {
        my %entry = %from{$entKey};
        %languages{%entry<name>} = True;
    }
    return %languages.keys.sort.join(', ');
}

#| Outputs data from a HandBrakeCLI --scan (as returned by performHDBScan) and exits
method outputScanData (:$min-duration)
{
    print "Scanning disc...";
    my %discMeta = self.getDiscMetadata();
    my %scanData = self.scan();
    print "done\n";
    say "Disc reports its name as %discMeta<LABEL> (UUID "~%discMeta<UUID>~')';
    my @main = ();
    if $.movie
    {
        @main = self.resolveTitles(%scanData, :silent);
    }
    if self.backend.^name eq 'LazyBrake::backend::Auto'
    {
        say "Backend chosen: "~self.backend.chosenBackend;
    }
    for @(%scanData<titleOrder>) -> $title
    {
        my $discTitle = %scanData<titles>.{$title};
        print "Title $title";
        print sprintf(" (%-5d seconds [%02d:%02d:%02d])", $discTitle.totalInSeconds, $discTitle.Hours, $discTitle.Minutes, $discTitle.Seconds);
        if $discTitle.subtitles
        {
            print " (subtitles: "~ self.getHumanLanguageList($discTitle.subtitles) ~")";
        }
        if $discTitle.audio
        {
            print " (audio: "~ self.getHumanLanguageList($discTitle.audio) ~")";
        }
        if $discTitle.mainTitle || (@main.elems == 1 && @main[0] == $title)
        {
            if self.backend().guessedMainTitle
            {
                print " [MAIN TITLE] \{GUESSED\}";
            }
            else
            {
                print " [MAIN TITLE]";
            }
        }
        if $discTitle.chapterHackRecommended
        {
            print ' c';
            print "\n";
            say "  --chapter-hack is recommended on this disc. Consult the manpage.";
            say "  Chapter list for title $title:";
            for $discTitle.chapters.list -> $Chapter
            {
                print "    Chapter "~$Chapter.chapterNo~" ";
                print sprintf(" (%-5d seconds [%02d:%02d:%02d])", $Chapter.totalInSeconds, $Chapter.Hours, $Chapter.Minutes, $Chapter.Seconds);
                print "\n";
            }
        }
        elsif $discTitle.chapterHackPossible
        {
            print ' c'~"\n";
        }
        else
        {
            print "\n";
        }
    }
    exit 0;
}

#| Construct a file name
method buildFileName (Int :$title!, :$volume_id!, :$volume_uuid!, Str :$extension is copy, Str :$name!, :$chapter) returns Str
{
    if !$extension
    {
        $extension = self.backend().extension;
    }
    my @components;
    if $name.defined && $name.chars > 0
    {
        @components.push($name);
    }
    @components.push(sprintf "%02d",$title);
    if $chapter.defined
    {
        @components.push('C'~$chapter.chapterNo);
    }
    if $volume_id.defined && $volume_id.chars > 0
    {
        @components.push($volume_id);
    }
    if $volume_uuid.defined && $volume_uuid.chars > 0
    {
        @components.push($volume_uuid);
    }
    my $fname = @components.join('_')~'.'~$extension;
    # Clean up the filename
        # Remove leading dots
    $fname ~~ s/^\.+//;
        # Remove characters we don't want
    $fname ~~ s:g/
        | \\
        | \/
        | \s
        | \"
        | \(
        | \)
        | \[
        | \]
        | \'
        | \<
        | \>
        | \$
        # Also merge multiple _
        | _+
    /_/;
    return $fname;
}

#| Rip an array of titles, validating their size etc. once done and
#| re-ripping if needed
method ripTitles(:$name!, :$volume_id, :$volume_uuid, :%scanData!, :@titles!) returns LazyBrake::RipDiscStatus
{
    my $jobNumber = 0;
    my @ripped;
    my $largestRipped = 0;
    my $totalFileSize = 0;
    my @toRip;
    my $infoString = '';
    my $chapterHackInUse = False;
    my $hasEjected = False;
    for @titles.sort -> $title
    {
        if ! %scanData{'titles'}.{$title}.defined
        {
            self.errOut: "Title "~$title~" did not have metadata attached. This is likely a LazyBrake bug. Make sure you have the latest version, and if you do please report this, with the debug information above. There is a chance attempting the disc again will work.", :internalError;
        }
        my Bool $enableChapterHack = False;
        if %scanData{'titles'}.{$title}.chapterHackPossible && $.chapterHack
        {
            $enableChapterHack = True;
        }
        elsif %scanData{'titles'}.{$title}.chapterHackRecommended && $.autoChapterHack
        {
            $enableChapterHack = True;
            self.printd: "Auto-enabling chapter hack due to --auto-chapter-hack";
        }
        if $enableChapterHack
        {
            self.printd: "Using chapter hack";
            for %scanData{'titles'}.{$title}.chapters.list -> $Chapter
            {
                @toRip.push({
                    :$title,
                    chapter => $Chapter,
                });
                $infoString ~= ' '~$title~'c'~$Chapter.chapterNo;
                $chapterHackInUse = True;
                self.printd: "Queued title $title chapter "~$Chapter.chapterNo;
            }
            if $.chapterHackExpectedTitles > 0 && @toRip.elems != $.chapterHackExpectedTitles
            {
                self.errOut: "Chapter hack error: did not queue the expected number of titles. This is a bug.", canRecover => False;
            }
        }
        else
        {
            @toRip.push({
                :$title
            });
            $infoString ~= ' '~$title;
        }
    }
    if @toRip.elems > 1 || $.tv
    {
        if $chapterHackInUse
        {
            say "Queued "~@toRip.elems~" elements for ripping: "~$infoString;
        }
        else
        {
            say "Queued "~@toRip.elems~" titles for ripping: "~$infoString;
        }
    }
    my $errorMessage;
    my $success = True;
    for @toRip -> %ripEntry
    {
        $jobNumber++;
        my $title = %ripEntry<title>;
        my $chapter = %ripEntry<chapter>;
        my $result = self.ripTitle(
            :$name,
            :$volume_id,
            :$volume_uuid,
            :$title,
            :$chapter,
            discTitle => %scanData<titles>.{$title},
            jobTotal => @toRip.elems,
            :$jobNumber,
        );
        if $result.errorMessage.defined
        {
            $errorMessage = $result.errorMessage;
        }
        if $result.hasEjected
        {
            $hasEjected = True;
        }
        if !$result.success
        {
            $success = False;
        }
        my $filename = $result.filename;

        if !$filename.defined
        {
            next;
        }

        # FIXME: Should validate *here*
        if $filename.IO.e
        {
            my $duration = %scanData<titles>.{$title}.totalInSeconds;
            if $chapterHackInUse && $chapter.defined
            {
                $duration = $chapter.totalInSeconds;
            }
            my $bytesPerSecond = $filename.IO.s / $duration;

            @ripped.push({
                :$jobNumber,
                filename       => $filename,
                title          => $title,
                :$chapter,
                metadata       => %scanData<titles>.{$title},
                bytesPerSecond => $bytesPerSecond,
                duration       => $duration,
                fileSize       => $filename.IO.s,
            });
            if $bytesPerSecond > $largestRipped
            {
                $largestRipped = $bytesPerSecond;
            }
        }
    }
    if @toRip.elems > 1
    {
        @toRip = ();
        $jobNumber = 1;
        print "Validating files...";
        for @ripped -> %processed
        {
            if ! %processed<filename>.IO.e
            {
                self.printv: %processed<filename>~" disappeared before validation. Unable to validate rip."
            }
            elsif ! self.backend().validateFile(
                file          => %processed<filename>,
                largestRipped => $largestRipped,
                duration      => %processed<duration>,
                discTitle     => %processed<metadata>,
            )
            {
                my $filename = %processed<filename>;
                say "$filename: failed validation, even after re-rip.";
                say "It might not have been ripped properly, you should verify the file manually.";
                my $tmpStorageName = self.saveBackupFile($filename);
                if $tmpStorageName
                {
                    say "(keeping the most complete rip as "~$tmpStorageName~")";
                }
            }
            else
            {
                $totalFileSize += %processed<fileSize>;
            }
        }
        print "done\n";
    }
    # Calculate file size
    for @ripped -> %processed
    {
        $totalFileSize += %processed<fileSize>;
    }
    return LazyBrake::RipDiscStatus.new(
        :$success,
        :$hasEjected,
        :fileSize($totalFileSize),
        :$errorMessage
    );
}

#| Rip a single title
method ripTitle(Int :$title!, :$name!, :$volume_id!, :$volume_uuid!, :$discTitle!, :$jobNumber!, :$jobTotal!, :$chapter, Bool :$no-dvdnav) returns LazyBrake::RipSingleStatus
{
    my $i = 0;
    my $maxAttempts = 3;
    loop
    {
        my $hasEjected = False;
        if $i == 0
        {
            sleep(2);
        }
        $i++;

        my $filename = self.buildFileName(:$title, :$name, :$volume_id, :$volume_uuid, :$chapter);
        my $validationDuration = $discTitle.totalInSeconds;
        if $chapter
        {
            # Needs to use the chapter length rather than the title length in
            # chapterHack mode
            $validationDuration = $chapter.totalInSeconds;
        }
        if $filename.IO.e
        {
            if ($i == 1)
            {
                print "$filename: already exists (title $title)";
                if self.backend().validateFile(file => $filename, duration => $validationDuration)
                {
                    say ", skipping this title";
                    return LazyBrake::RipSingleStatus.new(
                        :$filename
                    );
                }
                say ", but it failed validation. Re-ripping.";
            }
            self.saveBackupFile($filename);
        }
        if !$.dry-run
        {
            my $outputTitleStr = $title;
            if self.backend().guessedMainTitle
            {
                $outputTitleStr ~= " (guessed)";
            }
            if $jobTotal > 1
            {
                print '['~(sprintf "%02d",$jobNumber)~'/'~(sprintf "%02d",$jobTotal)~'] ';
            }
            if $chapter.defined
            {
                say "Ripping title $outputTitleStr chapter "~$chapter.chapterNo~" to $filename:";
            }
            else
            {
                say "Ripping title $outputTitleStr to $filename:";
            }
            my $response = self.backend().ripSingleTitle(
                :$title,
                :$filename,
                :$chapter,
                :$discTitle,
                :$no-dvdnav,
                elementsToRip => $jobTotal,
                jobNumber => $jobNumber,
            );
            $hasEjected = $response.hasEjected;
            if ! $response.success
            {
                if self.backend().validateFile(file => $filename, duration => $validationDuration)
                {
                    self.printv: 'backend returned an error but the file validated, ignoring backend';
                    return LazyBrake::RipSingleStatus.new(
                        :$filename
                    );
                }
                if $response.doNotRetry
                {
                    self.autoOutputLog(:reason('backend error'));
                    say "backend returned an error and indicated that it is not possible to retry.";
                    return LazyBrake::RipSingleStatus.new(
                        filename => self.saveBackupFile($filename),
                        :$hasEjected,
                        :success(False),
                        errorMessage => $response.errorMessage,
                    );
                }
                if $i >= $maxAttempts
                {
                    self.autoOutputLog(:reason('backend error'));
                    say "backend returned an error. Retries have been exhausted.";
                    say "This could be due to a bug in the backend ripper or a bad disc.";
                    say "If you tried to rip subtitles you may wish to try without them.";
                    say "If you want more details, run lazybrake with --full-debug";
                    return LazyBrake::RipSingleStatus.new(
                        filename => self.saveBackupFile($filename),
                        :$hasEjected,
                        :success(False),
                        errorMessage => $response.errorMessage,
                    );
                }
                else
                {
                    self.autoOutputLog(:reason('backend error'));
                    say "backend ripper returned an error, will retry [attempt $i/$maxAttempts].";
                }
                next;
            }
            my $tmpStorageName = $response.tmpName;
            if !$filename.IO.e
            {
                if $i >= $maxAttempts
                {
                    self.errOut: "Fatal error during rip validation: target file ($filename) does not exist after $i retries. lazybrake is confused. Aborting this rip.", :canRecover, :internalError;
                }
                say "An error occurred during ripping, will retry [attempt $i/$maxAttempts].";
                next;
            }
            if !self.backend().validateFile(file => $filename, duration => $validationDuration, :$discTitle)
            {
                if ($tmpStorageName && $tmpStorageName.IO.s > $filename.IO.s)
                {
                    $filename.IO.unlink;
                    $tmpStorageName.IO.rename($filename);
                }
                next;
            }
            elsif $tmpStorageName
            {
                $tmpStorageName.IO.unlink;
            }
        }
        else
        {
            if $chapter.defined
            {
                say "Would have ripped title $title chapter "~$chapter.chapterNo~" to $filename:";
            }
            else
            {
                say "Would have ripped title $title to $filename";
            }
        }
        my $status = LazyBrake::RipSingleStatus.new(
            :$filename,
            :$hasEjected,
            :success
        );
        if $filename.IO.e
        {
            $status.fileSize = $filename.IO.s;
        }
        return $status;
    }
}

#| Do a rip loop
method ripLoop (Int :$disc-no! is rw, *@titles)
{
    $.looping = True;

    # Output a waiting message if there's no disk in the drive when we enter the loop
    if !self.isDiscPresent
    {
        say "Waiting for disc to be inserted...";
    }
    my $first = True;
    # We loop indefinetely
    while True
    {
        # Sleep for five second periods until we have a disc in the drive
        while ! self.isDiscPresent(:bypass-cache)
        {
            sleep(5);
        }
        # Generate a disc specific name
        my $discName = '';
        if $.name.defined && $.name.chars > 0
        {
            $discName = $.name~'-';
        }
        $discName ~= 'D'~sprintf("%02d",$disc-no);

        # Output a newline if this isn't the first turn through the loop (to
        # visually separate different discs)
        if !$first
        {
            say "";
        }
        else
        {
            $first = False;
        }

        # Run the disc generation
        try
        {
            CATCH
            {
                when LazyBrake::RecoverableError
                {
                    note .message;
                    say "Skipping this disc.";
                    my %meta;
                    my $vol;
                    try
                    {
                        %meta = self.getDiscMetadata();
                        $vol = %meta<LABEL>;
                    }
                    if !$vol
                    {
                        $vol = 'UnknownDisc'~$.discsAttempted;
                    }
                    @.errorDiscs.push($vol);
                    self.eject();
                    self.runExecOnDone(.message);
                    sleep 2;
                }
            }
            self.handleDisc(
                name => $discName,
                :$disc-no,
                @titles
            );
        }

        $disc-no++;
        # Give the drive two seconds to finish ejecting before we poll it
        sleep 2;
    }
}
